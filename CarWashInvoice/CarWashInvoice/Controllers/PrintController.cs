﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarWashInvoice.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CarWashInvoice.Controllers
{
    public class PrintController : ApiController
    {
        PrintDAL ObjDAL = new PrintDAL();
        NumberToWordsController vary = new NumberToWordsController();
        public string QN;

        //PREREQUISITE FORM
        //[AllowAnonymous]
        [Route("Print/Invoice")]
        [HttpGet]

        public HttpResponseMessage Invoice(string service_ProductId)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            try
            {
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();
                DataTable dt3 = new DataTable();

                DataSet ds = ObjDAL.Invoice(service_ProductId);
                var rd = new ReportDocument();

                rd.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/Report/InvoiceRpt.rpt"));

                dt1 = ds.Tables[0];
                dt2 = ds.Tables[1];
                dt3 = ds.Tables[2];

                var TotalAmt = dt2.Rows[0]["FinalInvAmt"] ?? 0;

                var FinalInvAmt = vary.ConvertNumbertoWords(Convert.ToInt32(TotalAmt));

                dt2.Rows[0]["AmountInWords"] = FinalInvAmt;

                rd.Database.Tables["CompanyMaster"].SetDataSource(dt1);
                rd.Database.Tables["ServiceProductMain"].SetDataSource(dt2);
                rd.Database.Tables["ServiceProductSub"].SetDataSource(dt3);
      

                MemoryStream ms = new MemoryStream();

                using (var stream = rd.ExportToStream(ExportFormatType.PortableDocFormat))
                {
                    stream.CopyTo(ms);
                }

                result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(ms.ToArray())
                };
                result.Content.Headers.ContentDisposition =
                    new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                    {
                        FileName = service_ProductId + ".pdf"
                    };
                result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

            }
            catch (Exception ex)
            {
                 ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);               
            }
            return result;
        }
    }
}
