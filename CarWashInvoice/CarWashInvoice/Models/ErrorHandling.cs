﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CarWashInvoice.Models
{
    public class ErrorHandling
    {
        public static object WriteError(string Exception)
        {

            try
            {
                DateTime dt = new DateTime();
                dt = DateTime.Now.Date;
                string LOGDATE = null;
                string strPath = null;
                strPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLog");
                System.IO.Directory.CreateDirectory(strPath);
                LOGDATE = dt.ToString("MMM d yyyy");
                StreamWriter stwriter = new StreamWriter(strPath + "/" + "ErrorLog" + LOGDATE + ".txt", true);
                stwriter.WriteLine("*********************************************************************");
                stwriter.WriteLine("");
                stwriter.WriteLine("DATE TIME  :" + DateTime.Now.ToString());
                stwriter.WriteLine("ERROR INFO :" + Exception.ToString());
                stwriter.WriteLine("");
                stwriter.WriteLine("");
                stwriter.Flush();
                stwriter.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}