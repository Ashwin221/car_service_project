﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CarWashInvoice.Models
{
    public class PrintDAL
    {
        public DataSet Invoice(string Service_ProductId)
        {
            DataSet dt = new DataSet();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "PrintSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "Invoice");
                objSql.AddParameter(objCmd, "@Service_ProductId", SqlDbType.VarChar, ParameterDirection.Input, 100, Service_ProductId);
                objda.SelectCommand = objCmd;
                objCmd.CommandTimeout = 3000000;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
    }
}