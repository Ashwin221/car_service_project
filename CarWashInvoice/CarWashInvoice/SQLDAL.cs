﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CarWashInvoice
{
    public class SQLDAL : IDisposable
    {
        private SqlConnection mobjConn;
        private SqlTransaction mobjTran;
        string strConn = ConfigurationManager.ConnectionStrings["CarWash"].ConnectionString;
        public SQLDAL()
        {
            try
            {
                mobjConn = CreateConnection(strConn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                TerminateConnection();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void TerminateConnection()
        {
            try
            {
                if (!(mobjConn == null))
                {
                    if (mobjConn.State == ConnectionState.Open)
                    {
                        mobjConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mobjConn = null;
            }
        }

        public SqlConnection CreateConnection(string strConnection)
        {
            SqlConnection objCon;
            try
            {
                objCon = new SqlConnection(strConnection);
                objCon.Open();
                return objCon;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
        }

        public SqlCommand AddParameter(SqlCommand Command, String ParamName, SqlDbType PrmType,
                                    ParameterDirection PrmDirection, int intSize, Object Value)
        {
            try
            {
                Command.Parameters.Add(new SqlParameter(ParamName, PrmType, intSize, PrmDirection, false, Byte.MinValue, Byte.MinValue, null, DataRowVersion.Current, Value));
                return Command;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public SqlCommand AddParameter(SqlCommand Command, String ParamName, Object Value)
        {
            try
            {
                Command.Parameters.Add(new SqlParameter(ParamName, Value));
                return Command;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlCommand AddParameter(SqlCommand Command, String ParamName, SqlDbType PrmType)
        {
            try
            {
                Command.Parameters.Add(new SqlParameter(ParamName, PrmType));
                return Command;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlCommand AddParameter(SqlCommand Command, String ParamName, SqlDbType PrmType, int Size)
        {
            try
            {
                Command.Parameters.Add(new SqlParameter(ParamName, PrmType, Size, null));
                return Command;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet CreateDataset(string strTblName, SqlCommand Command)
        {
            SqlDataAdapter objDA = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {

                objDA.SelectCommand = Command;
                objDA.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDA = null;
            }
        }

        public DataTable CreateDataTable(SqlCommand Command)
        {
            SqlDataAdapter objDA = new SqlDataAdapter();
            DataTable dt = new DataTable();
            try
            {

                objDA.SelectCommand = Command;
                objDA.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDA = null;
            }
        }

        public void AppendDataset(string TableName, ref SqlCommand Command, ref DataSet objSqlDS)
        {

            SqlDataAdapter objSqlDA = new SqlDataAdapter();

            try
            {
                objSqlDA.SelectCommand = Command;

                if (objSqlDS == null == false)
                {
                    if (string.Compare(TableName, string.Empty) != 0)
                    {
                        objSqlDA.Fill(objSqlDS, TableName);
                    }
                    else
                    {
                        objSqlDA.Fill(objSqlDS);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objSqlDA = null;
            }

        }

        public SqlCommand CreateCommand(SqlCommand Command, CommandType CmdType, string CmdText)
        {
            try
            {
                Command = new SqlCommand();
                {
                    Command.Connection = mobjConn;
                    Command.CommandType = CmdType;
                    Command.CommandText = CmdText;
                }
                return Command;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void SetCmdText(ref SqlCommand Command, string CmdText)
        {

            try
            {
                Command.CommandText = CmdText;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void BeginTransaction()
        {

            try
            {
                if (mobjConn == null == false)
                {
                    if (mobjConn.State == ConnectionState.Open)
                    {
                        mobjTran = mobjConn.BeginTransaction(IsolationLevel.ReadCommitted);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public SqlTransaction GetTransaction()
        {
            SqlTransaction functionReturnValue = default(SqlTransaction);

            try
            {
                functionReturnValue = mobjConn.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;

        }

        public Int64 ExecuteQuery(SqlCommand Command)
        {
            Int64 functionReturnValue = default(Int64);

            try
            {
                Command.Transaction = mobjTran;
                functionReturnValue = Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;
        }
        public void EndTransaction(bool TranSuccess)
        {

            try
            {
                if ((mobjTran != null))
                {
                    if (TranSuccess == true)
                    {
                        mobjTran.Commit();
                    }
                    else
                    {
                        mobjTran.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mobjTran = null;
            }

        }

        public void RemoveParameters(ref SqlCommand Command)
        {

            try
            {
                {
                    while (!(Command.Parameters.Count == 0))
                    {
                        Command.Parameters.RemoveAt(0);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public DataTable ReturnUpdatedTable(ref SqlDataAdapter objDA, ref DataSet objDS, ref SqlCommand objCommand)
        {
            DataTable functionReturnValue = default(DataTable);

            DataViewRowState objDVRS = default(DataViewRowState);

            DataTable objTbl = objDS.Tables[0];
            int intStatus = 0;

            try
            {
                objDVRS = DataViewRowState.Added | DataViewRowState.ModifiedCurrent;
                foreach (DataRow objRow1 in objTbl.Select("", "", objDVRS))
                {
                    switch (objRow1.RowState)
                    {
                        case DataRowState.Added:
                            objDA.InsertCommand = objCommand;
                            break;
                        case DataRowState.Modified:
                            objDA.UpdateCommand = objCommand;
                            break;
                        case DataRowState.Deleted:
                            objDA.DeleteCommand = objCommand;
                            break;
                    }
                }
                intStatus = objDA.Update(objDS, "T1");
                objDS.Clear();
                objDA.Fill(objTbl);
                functionReturnValue = objTbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objTbl = null;

            }
            return functionReturnValue;
        }
    }
}