export const environment = {
  production: true,
  // Local  -------for 184 -----------
  //  api_url: 'https://localhost:44329/',
  //  print_url: 'https://localhost:44359/',

  // Local  -------for client--------
  // api_url: 'https://localhost:44364/',
  //print_url: 'https://localhost:44359/',

  //Test
  // api_url: 'http://184.168.121.23:8083/',
  // print_url: 'http://184.168.121.23:8084/',

  //184 - Live
  // api_url: 'http://184.168.121.23:8081/',         
  // print_url: 'http://184.168.121.23:8082/'

  //Client - Live
  // api_url: 'http://127.0.0.1:8001/',
  // print_url: 'http://127.0.0.1:8002/'

};
