import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { ViewCustomerMasterComponent } from './view-customer-master/view-customer-master.component';
import { CreateCustomerMasterComponent } from './create-customer-master/create-customer-master.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [ViewCustomerMasterComponent, CreateCustomerMasterComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule
  ]
})
export class CustomerModule { }
