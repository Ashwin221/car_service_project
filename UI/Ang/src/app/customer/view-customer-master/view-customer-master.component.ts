import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-view-customer-master',
  templateUrl: './view-customer-master.component.html',
  styleUrls: ['./view-customer-master.component.css'],
  providers: [DatePipe]
})
export class ViewCustomerMasterComponent implements OnInit {
  CustomerList: any = [];
  Visible: boolean = false;
  SubmittedRemarks: Boolean = false;
  Remarks: any;//NgModel
  CustomerId: any;

  @ViewChild('exportExcel') ExportExcel: ElementRef;
  constructor(private MasterSer: MasterService, private router: Router, private spinnerServ: NgxSpinnerService,
    private SharedSer: SharedService, private datepipe: DatePipe) { }

  ngOnInit(): void {
    this.GetCustomerMaster();
  }

  GetCustomerMaster() {
    this.CustomerList = [];
    this.Visible = false;
    this.spinnerServ.show();
    this.MasterSer.GetCustomerMaster().subscribe((data: any) => {
      if (data.success) {
        this.CustomerList = data.getCustomerMaster;
        this.Visible = true;
        this.spinnerServ.hide();
      }
      else {
        this.CustomerList = [];
        this.Visible = true;
        this.spinnerServ.hide();
      }
    })
  }

  EditDelete(Key, CustomerId) {
    this.SubmittedRemarks = false;
    this.Remarks = "";
    if (Key == "Edit") {
      this.router.navigate(["customer", "create-customer-master"], { queryParams: { customerId: CustomerId } })
    }
    this.CustomerId = CustomerId;
  }

  Delete() {
    this.SubmittedRemarks = true;
    if (this.Remarks == '') {
      return
    }
    this.spinnerServ.show();
    this.MasterSer.DeleteCustomerMaster(this.CustomerId, localStorage.getItem("userId"), this.Remarks).subscribe((data: any) => {
      if (data.success) {
        this.SharedSer.showNotification('top', 'center', 'Customer Details Deleted Successfully', 'danger');
      }
      else {
        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
      }
      this.spinnerServ.hide();
      this.GetCustomerMaster();
      document.getElementById('closeInActiveUnitModal').click();
    })
  }

  Excel() {
    if (this.CustomerList.length == 0) {
      this.SharedSer.showNotification('top', 'center', 'No Records Found to Export Excel', 'danger');
      return;
    }
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.ExportExcel.nativeElement, { raw: true });
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'ProductSales');
    XLSX.writeFile(wb, 'CustomerDetails_' + this.datepipe.transform(new Date(), "dd-MMM-yyyy") + '.xlsx');

  }
}
