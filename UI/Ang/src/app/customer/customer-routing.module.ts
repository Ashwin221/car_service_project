import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCustomerMasterComponent } from './view-customer-master/view-customer-master.component';
import { CreateCustomerMasterComponent } from './create-customer-master/create-customer-master.component';


const routes: Routes = [
  { path: "view-customer-master", component: ViewCustomerMasterComponent },
  { path: "create-customer-master", component: CreateCustomerMasterComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
