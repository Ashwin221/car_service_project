import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-create-customer-master',
  templateUrl: './create-customer-master.component.html',
  styleUrls: ['./create-customer-master.component.css']
})
export class CreateCustomerMasterComponent implements OnInit {

  CustomerId: any = 0;
  CustomerForm: FormGroup;
  MakeForm: FormGroup;
  ModelForm: FormGroup;
  MakeMaster: any = [];
  ModelMaster: any = [];
  CustomerSubList: any = [];
  Submitted: boolean = false;
  MakeNameExists: boolean = false;
  ModelNameExists: boolean = false;
  menuList: any = [];
  LoginUserId: any = 0;
  MakeSubmitted: boolean = false;
  ModelSubmitted: boolean = false;
  CustomerList: any = [];
  MobileNoExists: boolean = false;
  Type: any;
  Visible: boolean = true;

  constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService,
    private router: Router, private ActivatedRoute: ActivatedRoute, private location: Location, private SpinnerSer: NgxSpinnerService) {
    this.CustomerForm = this.fb.group({
      customerNo: ['', Validators.required],
      customerName: ['', Validators.required],
      mobileNo: ['', Validators.required],
      address: ['', Validators.required],
    });
    this.MakeForm = this.fb.group({
      MakeName: ['', Validators.required]
    });
    this.ModelForm = this.fb.group({
      make: [null, Validators.required],
      model: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.GetCustomerMaster();
    if (localStorage.getItem('LoginUserDtls') != null) {
      var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
      this.menuList = LoginUserDtls[0].allocateMenu.split(',');
      if (localStorage.getItem('UserType') == "User") {
        this.LoginUserId = localStorage.getItem('userId');
      }
    }
    this.GetMakeMaster();
    this.SetInitial();
    if (this.ActivatedRoute.snapshot.queryParamMap.get("Type") != null) {
      this.Type = this.ActivatedRoute.snapshot.queryParamMap.get("Type");
      this.Visible = (this.Type != "ProductsOnly") ? true : false;
    }
    if (this.ActivatedRoute.snapshot.queryParamMap.get("customerId") != null) {
      this.CustomerId = this.ActivatedRoute.snapshot.queryParamMap.get("customerId");
      this.GetCustomerMasterById();
    }
    else {
      this.GetCustomerNo();
    }
  }

  Back() {
    this.location.back();
  }

  OnchangeVechileDtls() {
    this.Visible = !this.Visible;
    if (this.Visible) {
      this.CustomerSubList = [];
      this.SetInitial();
    }
  }

  GetCustomerMasterById() {
    this.SpinnerSer.show();
    this.MasterSer.GetCustomerMasterById(this.CustomerId).subscribe((data: any) => {
      if (data.success) {
        this.Visible = data.getCustomerMasterById[0].visible;
        this.CustomerSubList = (this.Visible == true) ? data.getCustomerMasterById : [];
        this.CustomerForm.patchValue({
          customerNo: data.getCustomerMasterById[0].customerNo,
          customerName: data.getCustomerMasterById[0].customerName,
          mobileNo: data.getCustomerMasterById[0].mobileNo,
          address: data.getCustomerMasterById[0].address
        });
        this.SpinnerSer.hide();
      }
    });
  }


  GetCustomerNo() {
    this.MasterSer.GetCustomerNo().subscribe((data: any) => {
      if (data.success) {
        this.CustomerForm.controls['customerNo'].setValue(data.customerNo);
      }
    })
  }

  GetCustomerMaster() {
    this.MasterSer.GetCustomerMaster().subscribe((data: any) => {
      if (data.success) {
        this.CustomerList = data.getCustomerMaster;
      }
    })
  }

  OnChangeMobilNo() {
    // this.MobileNoExists = this.CustomerList.some((i: any) => i.mobileNo == this.CustomerForm.controls['mobileNo'].value && i.customerId !== this.CustomerId);

    var k = window.setInterval(() => {
      if (this.CustomerForm.controls['mobileNo'].value.toString().length == 10) {
        var CustomerList = this.CustomerList.filter((i: any) => i.mobileNo == this.CustomerForm.controls['mobileNo'].value && i.customerId !== this.CustomerId);
        if (CustomerList.length != 0) {
          this.CustomerId = CustomerList[0].customerId
          this.GetCustomerMasterById();
        }
        clearInterval(k);
      }
    }, 100)
  }

  SetInitial() {
    this.CustomerSubList.push({
      makeId: null,
      make: null,
      modelId: null,
      model: null,
      vehicleNo: ""
    })
  }

  GetMakeMaster() {
    this.MasterSer.GetMakeMaster().subscribe((data: any) => {
      if (data.success) {
        this.MakeMaster = data.getMakeMaster;
      }
    })
  }
  AddNewMakeOrModel(Key: any) {
    if (Key == 'Make') {
      this.MakeForm.controls['MakeName'].setValue("");
      this.MakeSubmitted = false;
      this.MakeNameExists = false;
      $('#AddMakeModal').on('shown.bs.modal', function () {
        $('#addmake').focus()
      });
    }
    if (Key == 'Model') {
      this.ModelForm.controls['make'].setValue(null);
      this.ModelForm.controls['model'].setValue("");
      this.ModelSubmitted = false;
      this.ModelNameExists = false;
    }
  }

  OnChangeMakeName() {
    this.MakeNameExists = this.MakeMaster.some((i: any) => i.make.toLowerCase() == this.MakeForm.controls['MakeName'].value.trim().toLowerCase());
  }

  SaveMake() {
    this.MakeSubmitted = true;
    if (this.MakeForm.invalid) {
      return;
    }
    let inputObj = {
      makeId: 0,
      make: this.MakeForm.controls['MakeName'].value.trim(),
      createdBy: this.LoginUserId
    }
    this.SpinnerSer.show();
    this.MasterSer.InsertMakeMaster(inputObj).subscribe((data: any) => {
      if (data.success) {

        this.SharedSer.showNotification('top', 'center', "Make Details Saved Successfully.", "success");
      } else {

        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
      }
      this.SpinnerSer.hide();
      this.GetMakeMaster();
      document.getElementById('closeAddMakeModal').click();
    })
  }

  OnChangeMake(MakeId: any, i: any) {
    this.CustomerSubList[i].makeId = MakeId.makeId;
    this.CustomerSubList[i].make = MakeId.make;
    this.MasterSer.GetModelMasterByMakeId(MakeId.makeId).subscribe((data: any) => {
      if (data.success) {
        this.ModelMaster = data.getModelMasterByMakeId;
      }
    })
  }

  OnChangeModelName() {
    this.ModelNameExists = this.ModelMaster.some((i: any) => i.make == this.ModelForm.controls['make'].value.make && i.model.toLowerCase() == this.ModelForm.controls['model'].value.trim().toLowerCase());
  }

  OnChangeModel(ModelId: any, i: any) {
    this.CustomerSubList[i].modelId = ModelId.modelId;
    this.CustomerSubList[i].model = ModelId.model;
  }

  SaveModel() {
    this.ModelSubmitted = true;
    if (this.ModelForm.invalid || this.ModelNameExists) {
      return;
    }

    let inputObj = {
      modelId: 0,
      makeId: this.ModelForm.controls['make'].value['makeId'],
      make: this.ModelForm.controls['make'].value['make'],
      model: this.ModelForm.controls['model'].value.trim(),
      createdBy: this.LoginUserId
    }

    this.SpinnerSer.show();
    this.MasterSer.InsertModelMaster(inputObj).subscribe((data: any) => {
      if (data.success) {

        this.SharedSer.showNotification('top', 'center', "Model Details Saved Successfully.", "success");
      } else {

        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
      }
      this.SpinnerSer.hide();
      document.getElementById('closeAddModelModal').click();
      this.CustomerSubList = this.CustomerSubList.filter((k: any) => k.modelId != null);
      // if (this.CustomerSubList.length) {
      //   this.SetInitial();
      // }
      this.ModelMaster = [];

      this.SetInitial();
    })
  }

  OnChangeVehicleNo(VehicleNo: any, i: any) {
    const isVehicleNoExists = this.CustomerSubList.some(({ vehicleNo }, index) => index != i && vehicleNo === VehicleNo);
    if (isVehicleNoExists) {
      this.CustomerSubList[i].vehicleNo = "";
      this.SharedSer.showNotification('top', 'center', "VehicleNo Already Exists", "info");
    }
  }

  PushRowToFamilyTable(i) {
    if (this.CustomerSubList.find((f, index) => index != i && (f.makeId == null ||
      f.modelId == null || f.vehicleNo == '' || (f.vehicleNo).trim() == ''))) {
      this.SharedSer.showNotification('top', 'center', "Please Fill All Required Fields", "info");
      return;
    }
    this.SetInitial();
    this.ModelMaster = [];
  }

  DeleteFamilyTableRow(i) {
    this.CustomerSubList.splice(i, 1);
  }



  Save() {
    this.Submitted = true;
    if (this.CustomerForm.invalid) {
      // this.SharedSer.showNotification('top', 'center', "Please Fill All Required Fields", "info");
      return;
    }
    if (this.Visible) {
      if ((this.CustomerSubList.find((f) => (f.makeId == null ||
        f.modelId == null || f.vehicleNo == '')))) {
        this.SharedSer.showNotification('top', 'center', "Please Fill All Required Fields", "info");
        return;
      }
    }
    if (this.Visible) {
      let inputObj = {
        customerId: Number(this.CustomerId),
        customerNo: this.CustomerForm.controls['customerNo'].value.trim(),
        customerName: this.CustomerForm.controls['customerName'].value.trim(),
        mobileNo: this.CustomerForm.controls['mobileNo'].value.toString(),
        address: this.CustomerForm.controls['address'].value.trim(),
        createdBy: Number(localStorage.getItem("userId")),
        customerSub: this.CustomerSubList,
      }
      console.log(inputObj)
      this.MasterSer.InsertCustomerMaster(inputObj).subscribe((data: any) => {
        if (data.success) {
          this.SharedSer.showNotification("top", "center", "Customer Details Saved Successfully", "success")
          this.location.back();
        }
      });
    }
    else {
      let inputObj = {
        customerId: this.CustomerId,
        customerNo: this.CustomerForm.controls['customerNo'].value.trim(),
        customerName: this.CustomerForm.controls['customerName'].value.trim(),
        mobileNo: this.CustomerForm.controls['mobileNo'].value.toString(),
        address: this.CustomerForm.controls['address'].value.trim(),
        createdBy: Number(localStorage.getItem("userId")),
      }
      console.log(inputObj)
      this.MasterSer.InsertCustomerMasterWthoutVehicleDtls(inputObj).subscribe((data: any) => {
        if (data.success) {
          this.SharedSer.showNotification("top", "center", "Customer Details Saved Successfully", "success")
          this.location.back();
        }
      });
    }
  }


}

