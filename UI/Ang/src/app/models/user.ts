import { Role } from "./role";
export class User {
    userId: number;
    fullName: string;
    email: string;
    pwd: string;
    role: Role;
    token?: string;
}