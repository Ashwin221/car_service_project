import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
    //{path:"sourcemaster", component: SourcemasterComponent},


    {
        path: '',
        redirectTo: 'pages/login',
        pathMatch: 'full'
    },

    {
        path: '',
        component: AdminLayoutComponent,
        children: [


            {
                path: 'master',
                loadChildren: './master/master.module#MasterModule',
            },
            {
                path: 'customer',
                loadChildren: './customer/customer.module#CustomerModule',
            },
            {
                path: 'service',
                loadChildren: './service/service.module#ServiceModule',
            },
            {
                path: 'reports',
                loadChildren: './reports/reports.module#ReportsModule',
            },

        ]
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        }]
    }
];
