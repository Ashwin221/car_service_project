import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagesRoutes } from './pages.routing';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login/login.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PagesRoutes),
        FormsModule,
        ReactiveFormsModule,HttpClientModule
    ],
    declarations: [
        LoginComponent
    ]
})

export class PagesModule {}
