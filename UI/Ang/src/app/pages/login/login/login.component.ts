import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { SharedService } from 'app/Services/shared.service';
import { LoginService } from 'app/Services/login.service';


@Component({
  //  moduleId: module.id,
  selector: 'login-cmp',
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
  focus;
  focus1;
  focus2;
  private toggleButton;
  private sidebarVisible: boolean;
  private nativeElement: Node;
  LoginForms: FormGroup
  submitted: boolean = false
  showPassword: boolean = true
  menuList: any;
  @ViewChild('closeBtn') closeBtn: ElementRef;


  constructor(private element: ElementRef, private fb: FormBuilder,
    private router: Router, private sharedService: SharedService, private el: ElementRef,
    private loginSer: LoginService
  ) {

    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
    this.LoginForms = this.fb.group({
      UserType: ['', Validators.required],
      UserName: ['', Validators.required],
      Password: ['', Validators.required]
    })


  }


  checkFullPageBackgroundImage() {
    var $page = $('.full-page');
    var image_src = $page.data('image');

    if (image_src !== undefined) {
      var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
      $page.append(image_container);
    }
  };


  ngOnInit() {

    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="UserName"]');
    invalidControl.focus();
    this.LoginForms.controls['UserType'].setValue("Admin")
    this.checkFullPageBackgroundImage();
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

    setTimeout(function () {
      // after 1000 ms we add the class animated to the login/register card
      $('.card').removeClass('card-hidden');
    }, 700)
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
  }
  sidebarToggle() {
    var toggleButton = this.toggleButton;
    var body = document.getElementsByTagName('body')[0];
    var sidebar = document.getElementsByClassName('navbar-collapse')[0];
    if (this.sidebarVisible == false) {
      setTimeout(function () {
        toggleButton.classList.add('toggled');
      }, 500);
      body.classList.add('nav-open');
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove('toggled');
      this.sidebarVisible = false;
      body.classList.remove('nav-open');
    }
  }
  Login() {
    this.submitted = true;
    if (this.LoginForms.invalid) {
      return;
    }
    // if (this.LoginForms.controls['UserType'].value == "Admin" && this.LoginForms.controls['UserName'].value == "Admin"
    //   && this.LoginForms.controls['Password'].value == "Admin@123") {
    //   localStorage.setItem("LoginUserDtls", JSON.stringify({
    //     "userId": 1,
    //     "name": "Admin",
    //     "userName": "Admin",
    //     "userType": "Admin",
    //     "password": "Admin@123"
    //   }));
    //   localStorage.setItem("UserType", "Admin");
    //   localStorage.setItem("UserName", "Admin");
    //   localStorage.setItem("userId", "1");
    //   this.router.navigate(['/master/view-company-master']);
    // }
    // else {
    //   this.sharedService.showNotification('top', 'center', "Enter valid UserName/Password.", "danger");
    // }
    this.loginSer.GetLoginUserDetails(this.LoginForms.controls['UserType'].value, this.LoginForms.controls['UserName'].value, this.LoginForms.controls['Password'].value).subscribe((data: any) => {
      if (data.success) {
        // sessionStorage.setItem("LoginUserDtls", JSON.stringify(data.loginList));
        // sessionStorage.setItem("UserType", this.LoginForms.controls['UserType'].value);
        // sessionStorage.setItem("userName", this.LoginForms.controls['UserName'].value);
        localStorage.setItem("LoginUserDtls", JSON.stringify(data.getLoginUserDetails));
        localStorage.setItem("UserType", data.getLoginUserDetails[0].userType);
        localStorage.setItem("UserName", data.getLoginUserDetails[0].userName);
        localStorage.setItem("userId", data.getLoginUserDetails[0].userId);
        this.router.navigate(['/service/view-service']);
      }
      else {
        this.sharedService.showNotification('top', 'center', "Enter valid UserName/Password.", "danger");
      }
    })
  }
}
