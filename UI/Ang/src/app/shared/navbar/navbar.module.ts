import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
    imports: [RouterModule, CommonModule, NgxSpinnerModule],
    declarations: [NavbarComponent],
    exports: [NavbarComponent]
})

export class NavbarModule {

}
