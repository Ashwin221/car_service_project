import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MasterService {
  public url: string = environment.api_url;
  constructor(private http: HttpClient) { }


  //UnitMaster details
  public InsertUnitMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertUnitMaster", inputobj);
  }

  public GetUnitMaster() {
    return this.http.get(this.url + 'Master/GetUnitMaster')
  }

  public GetUnitDetailsById(UnitId) {
    return this.http.get(this.url + "Master/GetUnitMasterById?unitId=" + UnitId)
  }

  public DeleteUnitMaster(UnitId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteUnitMaster?unitId=" + UnitId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //GST Master Details
  public InsertGstMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertGSTMaster", inputobj);
  }
  public GetGstMaster() {
    return this.http.get(this.url + 'Master/GetGSTMaster')
  }

  public GetGstDetailsById(gstId) {
    return this.http.get(this.url + "Master/GetGSTMasterById?gstId=" + gstId)
  }

  public DeleteGstMaster(gstId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteGSTMaster?gstId=" + gstId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //MAKE Master Details
  public InsertMakeMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertMakeMaster", inputobj);
  }
  public GetMakeMaster() {
    return this.http.get(this.url + 'Master/GetMakeMaster')
  }

  public GetMakeDetailsById(makeId) {
    return this.http.get(this.url + "Master/GetMakeMasterById?makeId=" + makeId)
  }

  public DeleteMakeMaster(makeId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteMakeMaster?makeId=" + makeId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //Model Master
  public InsertModelMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertModelMaster", inputobj);
  }
  public GetModelMaster() {
    return this.http.get(this.url + 'Master/GetModelMaster')
  }

  public GetModelDetailsById(modelId) {
    return this.http.get(this.url + "Master/GetModelMasterById?modelId=" + modelId)
  }

  public DeleteModelMaster(modelId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteModelMaster?modelId=" + modelId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  public GetModelMasterByMakeId(MakeId) {
    return this.http.get(this.url + "Master/GetModelMasterByMakeId?makeId=" + MakeId)
  }

  //PaymentMode Master
  public InsertPaymentModeMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertPaymentModeMaster", inputobj);
  }
  public GetPaymentModeMaster() {
    return this.http.get(this.url + 'Master/GetPaymentModeMaster')
  }

  public GetPaymentModeDetailsById(paymentModeId) {
    return this.http.get(this.url + "Master/GetPaymentModeMasterById?paymentModeId=" + paymentModeId)
  }

  public DeletePaymentModeMaster(paymentModeId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeletePaymentModeMaster?paymentModeId=" + paymentModeId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //Product Master

  public InsertProductMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertProductMaster", inputobj);
  }
  public GetProductMaster() {
    return this.http.get(this.url + 'Master/GetProductMaster')
  }

  public GetProductMasterById(productId) {
    return this.http.get(this.url + "Master/GetProductMasterById?productId=" + productId)
  }

  public DeleteProductMaster(productId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteProductMaster?productId=" + productId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //Employee Master

  public InsertEmployeeMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertEmployeeMaster", inputobj);
  }
  public GetEmployeeMaster() {
    return this.http.get(this.url + 'Master/GetEmployeeMaster')
  }

  public GetEmployeeMasterById(EmployeeId) {
    return this.http.get(this.url + "Master/GetEmployeeMasterById?EmployeeId=" + EmployeeId)
  }

  public DeleteEmployeeMaster(EmployeeId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteEmployeeMaster?employeeId=" + EmployeeId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //Service Master

  public InsertServiceMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertServiceMaster", inputobj);
  }
  public GetServiceMaster() {
    return this.http.get(this.url + 'Master/GetServiceMaster')
  }

  public GetServiceMasterById(ServiceId) {
    return this.http.get(this.url + "Master/GetServiceMasterById?serviceId=" + ServiceId)
  }

  public DeleteServiceMaster(ServiceId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteServiceMaster?serviceId=" + ServiceId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //User Master

  public GetMenuMasterDtls() {
    return this.http.get(this.url + "Master/GetMenuMaster")
  }

  public InsertUserMaster(inputobj) {
    return this.http.post(this.url + "Master/InsertUserMaster", inputobj);
  }
  public GetUserMaster() {
    return this.http.get(this.url + 'Master/GetUserMaster')
  }

  public GetUserMasterById(UserId) {
    return this.http.get(this.url + "Master/GetUserMasterById?userId=" + UserId)
  }

  public DeleteUserMaster(UserId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteUserMaster?userId=" + UserId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  //Customer Master

  public GetCustomerNo() {
    return this.http.get(this.url + "Master/GetCustomerNo")
  }

  public InsertCustomerMaster(inputObj: any) {
    return this.http.post(this.url + "Master/InsertCustomerMaster", inputObj)
  }

  public InsertCustomerMasterWthoutVehicleDtls(inputObj: any) {
    return this.http.post(this.url + "Master/InsertCustomerMasterWthoutVehicleDtls", inputObj)
  }

  public GetCustomerMaster() {
    return this.http.get(this.url + "Master/GetCustomerMaster")
  }

  public GetCustomerMasterById(CustomerId) {
    return this.http.get(this.url + "Master/GetCustomerMasterById?customerId=" + CustomerId)
  }

  public DeleteCustomerMaster(CustomerId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteCustomerMaster?customerId=" + CustomerId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }

  public GetStateMaster() {
    return this.http.get(this.url + "Master/GetStateMaster")
  }

  public GetDistrictMasterByStateId(StateId) {
    return this.http.get(this.url + "Master/GetDistrictMasterByStateId?stateId=" + StateId)
  }

  //Company Master

  public InsertCompanyMaster(inputObj: any) {
    return this.http.post(this.url + "Master/InsertCompanyMaster", inputObj)
  }

  public GetCompanyMaster() {
    return this.http.get(this.url + "Master/GetCompanyMaster")
  }

  public GetCompanyMasterById(CompanyId: any) {
    return this.http.get(this.url + "Master/GetCompanyMasterById?companyId=" + CompanyId)
  }

  public DeleteCompanyMaster(CompanyId, CreatedBy, Remarks) {
    return this.http.get(this.url + "Master/DeleteCompanyMaster?companyId=" + CompanyId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
  }
}
