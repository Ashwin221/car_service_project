
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ServiceproductService {
    public url: string = environment.api_url;
    public Print_url: string = environment.print_url;
    constructor(private http: HttpClient) { }

    public GetVehicleRegNoDtls(CustomerId) {
        return this.http.get(this.url + "ServiceProduct/GetVehicleRegNoDtls?CustomerId=" + CustomerId)
    }

    public InsertServiceProduct(inputObj) {
        return this.http.post(this.url + "ServiceProduct/InsertServiceProduct", inputObj)
    }
    public GetProductServiceDtls() {
        return this.http.get(this.url + "ServiceProduct/GetProductServiceDtls")
    }
    public GetProductServiceDtlsById(ServiceProductId) {
        return this.http.get(this.url + "ServiceProduct/GetProductServiceDtlsById?serviceProductId=" + ServiceProductId)
    }

    public DeleteProductServiceMaster(ServiceProductId, CreatedBy, Remarks) {
        return this.http.get(this.url + "ServiceProduct/DeleteProductServiceMaster?serviceProductId=" + ServiceProductId + '&createdBy=' + CreatedBy + '&remarks=' + Remarks)
    }

    public EligibleForFreeWash(MobileNo, Category) {
        return this.http.get(this.url + "ServiceProduct/EligibleForFreeWash?mobileNo=" + MobileNo + "&category=" + Category)
    }

    //Print

    public PrintInvoice(Service_ProductId: any) {
        return this.http.get(this.Print_url + "Print/Invoice?service_ProductId=" + Service_ProductId, { responseType: 'blob' })
    }
}
