import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  public url: string = environment.api_url;
  constructor(private http: HttpClient) { }


  //Services
  public GetServices(FromDate: any, ToDate: any) {
    return this.http.get(this.url + "Reports/GetServices?fromDate=" + FromDate + "&toDate=" + ToDate);
  }

  //Product Sales
  public GetProductSales(FromDate: any, ToDate: any) {
    return this.http.get(this.url + "Reports/GetProductSales?fromDate=" + FromDate + "&toDate=" + ToDate);
  }

  //Services By Employee
  public GetServicesByEmpId(FromDate: any, ToDate: any, EmployeeId: any) {
    return this.http.get(this.url + "Reports/GetServicesByEmpId?fromDate=" + FromDate + "&toDate=" + ToDate + "&employeeId=" + EmployeeId);
  }

  //Product Sales By Employee
  public GetProductSalesByEmpId(FromDate: any, ToDate: any, EmployeeId: any) {
    return this.http.get(this.url + "Reports/GetProductSalesByEmpId?fromDate=" + FromDate + "&toDate=" + ToDate + "&employeeId=" + EmployeeId);
  }

}
