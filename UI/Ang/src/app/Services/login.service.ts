import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public url: string = environment.api_url;
  constructor(private http: HttpClient) { }
  
  public GetLoginUserDetails(UserType, UserName, Password) {
    UserType="Admin";
    return this.http.get(this.url + "Login/GetLoginUserDetails?userType=" + UserType + "&userName=" + UserName + "&password=" + Password)
  }


}
