import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { User } from 'app/models/user';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private route: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }


  login(UserName: string, Password: string) {
    return this.http.post<any>(`${environment.api_url}/Login/loginValue`, { UserName, Password })
      .pipe(map(user => {
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;       
      }));
  }



  logout() {
    // remove user from local storage to log user out
    //localStorage.removeItem('currentUser');   
    //alert("fdgfg")
    localStorage.clear();
    //alert(JSON.stringify(localStorage.getItem('currentUser')))
    this.currentUserSubject.next(null);  
    this.route.navigate(['']);
  }



}
