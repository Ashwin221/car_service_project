import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = environment.api_url;
  header = { Authorization: `Bearer ${localStorage.currentUser != undefined ? JSON.parse(localStorage.currentUser).token : ''}` }

  constructor(private http: HttpClient) { }

  public GetLoginUserDetails(UserType, UserName, Password) {
    return this.http.get(this.url + "Login/GetLoginUserDetails?userType=" + UserType + "&userName=" + UserName + "&password=" + Password)
  }
}
