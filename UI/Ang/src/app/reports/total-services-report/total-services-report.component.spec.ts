import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalServicesReportComponent } from './total-services-report.component';

describe('TotalServicesReportComponent', () => {
  let component: TotalServicesReportComponent;
  let fixture: ComponentFixture<TotalServicesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalServicesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalServicesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
