import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ReportsService } from 'app/Services/reports.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';

@Component({
    selector: 'app-total-services-report',
    templateUrl: './total-services-report.component.html',
    styleUrls: ['./total-services-report.component.css'],
    providers: [DatePipe]
})
export class TotalServicesReportComponent implements OnInit {
    ServiceList: any = [];
    Visible: boolean = false;
    FromDate: any; //NgModel
    ToDate: any; //NgModel
    Submitted: boolean = false;

    @ViewChild('exportExcel') ExportExcel: ElementRef;
    constructor(private ReportsSer: ReportsService, private SpinnerSer: NgxSpinnerService,
        private datePipe: DatePipe, private SharedSer: SharedService) { }

    ngOnInit(): void {
        var date = new Date();
        this.FromDate = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth(), 1), "yyyy-MM-dd");
        this.ToDate = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth() + 1, 0), "yyyy-MM-dd");
        this.GetServices();
    }

    GetServices() {
        this.Submitted = true;

        if (this.FromDate == "" || this.ToDate == "") {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Date', 'danger');
            this.Visible = false;
            return
        }
        
        this.Visible = false;
        this.ServiceList = [];
        this.SpinnerSer.show();
        this.ReportsSer.GetServices(this.FromDate, this.ToDate).subscribe((data: any) => {
            if (data.success) {
                this.ServiceList = data.getServices;
                this.Visible = true;
                this.SpinnerSer.hide();
            }
            else {
                this.ServiceList = [];
                this.Visible = true;
                this.SpinnerSer.hide();
            }
        })
    }

    Excel() {

        if (this.FromDate == "" || this.ToDate == "") {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Date', 'danger');
            this.Visible = false;
            return
        }

        if (this.ServiceList.length == 0) {
            this.SharedSer.showNotification('top', 'center', 'No Records Found to Export Excel', 'danger');
            return;
        }
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.ExportExcel.nativeElement, { raw: true });
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Services');
        XLSX.writeFile(wb, 'Services_' + this.datePipe.transform(this.FromDate, 'dd-MM-yyyy') + '_to_' + this.datePipe.transform(this.ToDate, 'dd-MM-yyyy') + '.xlsx');
    }

}
