import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { TotalServicesReportComponent } from './total-services-report/total-services-report.component';
import { TotalProductSalesComponent } from './total-product-sales/total-product-sales.component';
import { EmployeeWiseProductSalesComponent } from './employee-wise-product-sales/employee-wise-product-sales.component';
import { EmployeeWiseServiceComponent } from './employee-wise-service/employee-wise-service.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [TotalServicesReportComponent, TotalProductSalesComponent, EmployeeWiseProductSalesComponent, EmployeeWiseServiceComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    NgSelectModule,
    FormsModule,
    DataTablesModule
  ]
})
export class ReportsModule { }
