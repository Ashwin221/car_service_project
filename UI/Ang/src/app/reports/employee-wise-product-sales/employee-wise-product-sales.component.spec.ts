import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeWiseProductSalesComponent } from './employee-wise-product-sales.component';

describe('EmployeeWiseProductSalesComponent', () => {
  let component: EmployeeWiseProductSalesComponent;
  let fixture: ComponentFixture<EmployeeWiseProductSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeWiseProductSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeWiseProductSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
