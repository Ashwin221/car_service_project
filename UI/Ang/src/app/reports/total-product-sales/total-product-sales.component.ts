import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ReportsService } from 'app/Services/reports.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';

@Component({
    selector: 'app-total-product-sales',
    templateUrl: './total-product-sales.component.html',
    styleUrls: ['./total-product-sales.component.css'],
    providers: [DatePipe]
})
export class TotalProductSalesComponent implements OnInit {
    ProductSalesList: any = [];
    Visible: boolean = false;
    FromDate: any; //NgModel
    ToDate: any; //NgModel
    Submitted: boolean = false;

    @ViewChild('exportExcel') ExportExcel: ElementRef;
    constructor(private ReportsSer: ReportsService, private SpinnerSer: NgxSpinnerService,
        private datePipe: DatePipe, private SharedSer: SharedService) { }

    ngOnInit(): void {
        var date = new Date();
        this.FromDate = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth(), 1), "yyyy-MM-dd");
        this.ToDate = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth() + 1, 0), "yyyy-MM-dd");
        this.GetProductSales();
    }

    GetProductSales() {
        this.Submitted = true;

        if (this.FromDate == "" || this.ToDate == "") {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Date', 'danger');
            this.Visible = false;
            return
        }
        
        this.Visible = false;
        this.ProductSalesList = [];
        this.SpinnerSer.show();
        this.ReportsSer.GetProductSales(this.FromDate, this.ToDate).subscribe((data: any) => {
            if (data.success) {
                this.ProductSalesList = data.getProductSales;
                this.Visible = true;
                this.SpinnerSer.hide();
            }
            else {
                this.ProductSalesList = [];
                this.Visible = true;
                this.SpinnerSer.hide();
            }
        })
    }
    Excel() {

        if (this.FromDate == "" || this.ToDate == "") {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Date', 'danger');
            this.Visible = false;
            return
        }

        if (this.ProductSalesList.length == 0) {
            this.SharedSer.showNotification('top', 'center', 'No Records Found to Export Excel', 'danger');
            return;
        }
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.ExportExcel.nativeElement, { raw: true });
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'ProductSales');
        XLSX.writeFile(wb, 'ProductSales_' + this.datePipe.transform(this.FromDate, 'dd-MM-yyyy') + '_to_' + this.datePipe.transform(this.ToDate, 'dd-MM-yyyy') + '.xlsx');

    }
}
