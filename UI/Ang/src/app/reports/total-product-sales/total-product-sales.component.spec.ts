import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalProductSalesComponent } from './total-product-sales.component';

describe('TotalProductSalesComponent', () => {
  let component: TotalProductSalesComponent;
  let fixture: ComponentFixture<TotalProductSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalProductSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalProductSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
