import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeWiseProductSalesComponent } from './employee-wise-product-sales/employee-wise-product-sales.component';
import { EmployeeWiseServiceComponent } from './employee-wise-service/employee-wise-service.component';
import { TotalProductSalesComponent } from './total-product-sales/total-product-sales.component';
import { TotalServicesReportComponent } from './total-services-report/total-services-report.component';


const routes: Routes = [
    {path:'total-services',component:TotalServicesReportComponent},
    {path:'total-product-sales',component:TotalProductSalesComponent},
    {path:'employee-wise-service',component:EmployeeWiseServiceComponent},
    {path:'employee-wise-product-sales',component:EmployeeWiseProductSalesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
