import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MasterService } from 'app/Services/master.service';
import { ReportsService } from 'app/Services/reports.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';

@Component({
    selector: 'app-employee-wise-service',
    templateUrl: './employee-wise-service.component.html',
    styleUrls: ['./employee-wise-service.component.css'],
    providers: [DatePipe]
})
export class EmployeeWiseServiceComponent implements OnInit {
    ServiceListByEmpId: any = [];
    Visible: boolean = false;
    FromDate: any; //NgModel
    ToDate: any; //NgModel
    EmployeeId: any; //NgModel
    Submitted: boolean = false;
    EmployeeList: any = [];
    EmployeeName: any = "";

    @ViewChild('exportExcel') ExportExcel: ElementRef;
    constructor(private ReportsSer: ReportsService, private SpinnerSer: NgxSpinnerService,
        private datePipe: DatePipe, private SharedSer: SharedService, private MasterSer: MasterService) { }

    ngOnInit(): void {
        var date = new Date();
        this.FromDate = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth(), 1), "yyyy-MM-dd");
        this.ToDate = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth() + 1, 0), "yyyy-MM-dd");
        this.GetServicesByEmpId();
        this.GetEmployeeMaster();
    }

    GetServicesByEmpId() {
        this.Submitted = true;
        if (this.FromDate == "" || this.ToDate == "") {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Date', 'danger');
            this.Visible = false;
            return
        }
        this.Visible = false;
        this.ServiceListByEmpId = [];
        this.SpinnerSer.show();
        this.ReportsSer.GetServicesByEmpId(
            this.FromDate,
            this.ToDate,
            this.EmployeeId
        ).subscribe((data: any) => {
            if (data.success) {
                this.ServiceListByEmpId = data.getServicesByEmpId;
                this.Visible = true;
                this.SpinnerSer.hide();
            }
            else {
                this.ServiceListByEmpId = [];
                this.Visible = true;
                this.SpinnerSer.hide();
            }
        })
    }

    GetEmployeeMaster() {
        this.MasterSer.GetEmployeeMaster().subscribe((data: any) => {
            if (data.success) {
                this.EmployeeList = data.getEmployeeMaster;
            }
        })
    }

    OnChangeEmployee(event: any) {
        this.EmployeeName = event.employeeName + '_'
    }

    Excel() {

        if (this.FromDate == "" || this.ToDate == "") {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Date', 'danger');
            this.Visible = false;
            return
        }
        
        if (this.ServiceListByEmpId.length == 0) {
            this.SharedSer.showNotification('top', 'center', 'No Records Found to Export Excel', 'danger');
            return;
        }
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.ExportExcel.nativeElement, { raw: true });
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Services');
        XLSX.writeFile(wb, this.EmployeeName + 'Services_' + this.datePipe.transform(this.FromDate, 'dd-MM-yyyy') + '_to_' + this.datePipe.transform(this.ToDate, 'dd-MM-yyyy') + '.xlsx');

    }

}
