import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeWiseServiceComponent } from './employee-wise-service.component';

describe('EmployeeWiseServiceComponent', () => {
  let component: EmployeeWiseServiceComponent;
  let fixture: ComponentFixture<EmployeeWiseServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeWiseServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeWiseServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
