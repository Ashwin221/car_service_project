import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateServiceComponent } from './create-service/create-service.component';
import { ViewServiceComponent } from './view-service/view-service.component';


const routes: Routes = [
    {path:'create-service',component:CreateServiceComponent},
    {path:'view-service',component:ViewServiceComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
