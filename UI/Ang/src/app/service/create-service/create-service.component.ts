import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterService } from 'app/Services/master.service';
import { ServiceproductService } from 'app/Services/serviceproduct.service';
import { SharedService } from 'app/Services/shared.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgxSpinnerService } from 'ngx-spinner';
import * as printJS from 'print-js';

@Component({
    selector: 'app-create-service',
    templateUrl: './create-service.component.html',
    styleUrls: ['./create-service.component.css'],
    providers: [DatePipe]
})
export class CreateServiceComponent implements OnInit {
    ServiceProductSub: {
        category: any,
        type: any,
        typeId: any,
        typeName: any,
        price: any,
        qty: any,
        amount: any,
        discount: any,
        isAvailGst: boolean,
        gstId: any,
        gstName: any,
        gstValue: any,
        taxableAmt: any,
        total: any,
        freeCarWash: boolean,
        freeBikeWash: boolean
    }[] = [];

    ServiceProductForm: FormGroup;
    ServiceProductId: any = 0;
    Submitted: boolean = false;
    CustomerList: any = [];
    VehicleRegNoLstList: any = [];
    ProductList: any = [];
    ServiceList: any = [];
    GstList: any = [];
    SumOfPrice: any = 0;
    SumOfDiscount: any = 0;
    SumOfTaxableAmt: any = 0;
    SumOfTotal: any = 0;
    EmployeeList: any = [];
    SalesDoneByList: any = [];
    ServiceDoneByList: any = [];
    LoginUserId: any = 0;
    dropdownSettings: IDropdownSettings = {};
    IsDisableServiceDoneBy: boolean = false;
    IsDisableSalesDoneBy: boolean = true;
    menuList: any = [];
    IsEligible: boolean = false;
    Content: any;
    Hidden: boolean = false;
    SumOfAmt: any = 0;
    SumOfQty: any = 1;
    ServiceEntered: boolean = false;

    constructor(private fb: FormBuilder, private MasterSer: MasterService, private ServiceProdSer: ServiceproductService,
        private SharedSer: SharedService, private SpinnerSer: NgxSpinnerService, private router: Router,
        private activateRoute: ActivatedRoute, private datePipe: DatePipe) {
        this.ServiceProductForm = this.fb.group({
            'Type': [''],
            'ServiceNo': [''],
            'Date': ['', Validators.required],
            'MobileNo': ['', Validators.required],
            'Customer': [null, Validators.required],
            'VehicleRegNo': [null],
            'Make': [''],
            'Model': [''],
            'DeliveryDateTime': ['', Validators.required],
            'OverAllTotal': [''],
            'DiscountFromTotal': [''],
            'FinalInvAmt': [''],
        })
    }

    async ngOnInit() {
        this.SpinnerSer.show();
        this.ServiceProductForm.controls['Date'].setValue(this.datePipe.transform(new Date(), 'yyyy-MM-dd'))
        await this.GetCustomerMaster();
        await this.GetServiceDetails();
        await this.GetProductMaster();
        await this.GetGstDetails();
        this.GetEmployeeMaster();
        this.dropdownSettings = {
            idField: 'employeeId',
            textField: 'employeeName'
        };

        if (this.activateRoute.snapshot.queryParamMap.get('ServiceProductId') != null) {
            this.ServiceProductId = this.activateRoute.snapshot.queryParamMap.get('ServiceProductId')
            this.GetProductServiceDtlsById();
        }
        else {
            this.ServiceProductForm.controls["Type"].setValue("ServiceAndProducts");
            this.OnChangeSelectType();
        }
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
        this.SpinnerSer.hide();
    }

    OnChangeSelectType() {
        if (this.ServiceProductForm.controls["Type"].value == "ProductsOnly") {
            this.ServiceProductForm.controls['VehicleRegNo'].clearValidators();
            this.ServiceProductForm.controls["VehicleRegNo"].updateValueAndValidity();
            this.ServiceProductForm.controls['Make'].clearValidators();
            this.ServiceProductForm.controls["Make"].updateValueAndValidity();
            this.ServiceProductForm.controls['Model'].clearValidators();
            this.ServiceProductForm.controls["Model"].updateValueAndValidity();
            this.Hidden = true;
            this.ServiceProductSub = [];
            this.SumOfDiscount = 0;
            this.SumOfPrice = 0;
            this.SumOfAmt = 0;
            this.SumOfQty = 1;
            this.SumOfTaxableAmt = 0;
            this.SumOfTotal = 0;
            this.AddRow(0);
        }
        else if (this.ServiceProductForm.controls["Type"].value == "ServiceAndProducts") {
            this.ServiceProductForm.controls['VehicleRegNo'].setValidators(Validators.required);
            this.ServiceProductForm.controls["VehicleRegNo"].updateValueAndValidity();
            this.ServiceProductForm.controls['Make'].setValidators(Validators.required);
            this.ServiceProductForm.controls["Make"].updateValueAndValidity();
            this.ServiceProductForm.controls['Model'].setValidators(Validators.required);
            this.ServiceProductForm.controls["Model"].updateValueAndValidity();
            this.Hidden = false;
            this.ServiceProductSub = [];
            this.SumOfDiscount = 0;
            this.SumOfPrice = 0;
            this.SumOfAmt = 0;
            this.SumOfQty = 1;
            this.SumOfTaxableAmt = 0;
            this.SumOfTotal = 0;
            this.AddRow(0);
        }
    }

    OnChangeMobilNo() {
        if (this.ServiceProductForm.controls['MobileNo'].value != '' && this.ServiceProductForm.controls['MobileNo'].value != null) {
            if (this.ServiceProductForm.controls['MobileNo'].value.toString().length == 10) {
                this.CustomerList = this.CustomerList.filter((k: any) => k.mobileNo == this.ServiceProductForm.controls['MobileNo'].value);
                if (this.CustomerList.length == 0) {
                    this.ServiceProductForm.reset();
                    this.ServiceProductForm.controls['Date'].setValue(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
                    this.ServiceProductForm.controls["Type"].setValue("ServiceAndProducts");
                    this.GetCustomerMaster();
                    this.SharedSer.showNotification("top", "center", "Please Enter Valid Mobile Number", "info");
                }
                else {
                    this.ServiceProductForm.controls['Customer'].setValue(this.CustomerList[0])
                    this.OnChangeCustomer();
                }
            }
            else {
                this.ServiceProductForm.controls['Customer'].setValue(null)
                this.OnChangeCustomer();
                this.ServiceProductSub = [];
                this.AddRow(0);
                this.IsEligible = false;
                this.SumOfDiscount = 0;
                this.SumOfPrice = 0;
                this.SumOfAmt = 0;
                this.SumOfQty = 1;
                this.SumOfTaxableAmt = 0;
                this.SumOfTotal = 0;
            }
        }
        else {
            this.ServiceProductForm.controls['Customer'].setValue(null)
            this.OnChangeCustomer();
            this.ServiceProductSub = [];
            this.AddRow(0);
            this.IsEligible = false;
            this.SumOfDiscount = 0;
            this.SumOfPrice = 0;
            this.SumOfAmt = 0;
            this.SumOfQty = 1;
            this.SumOfTaxableAmt = 0;
            this.SumOfTotal = 0;
        }
    }

    GetProductServiceDtlsById() {
        this.ServiceProdSer.GetProductServiceDtlsById(this.ServiceProductId).subscribe((data: any) => {
            if (data.success) {
                this.ServiceProductForm.patchValue({
                    'ServiceNo': data.getProductServiceDtlsByIdMain[0].serviceProductNo,
                    'Type': data.getProductServiceDtlsByIdMain[0].type,
                    'Date': data.getProductServiceDtlsByIdMain[0].date,
                    'MobileNo': data.getProductServiceDtlsByIdMain[0].mobileNo,
                    'Customer': this.CustomerList.find(f => f.customerId == data.getProductServiceDtlsByIdMain[0].customerId),
                    'DeliveryDateTime': data.getProductServiceDtlsByIdMain[0].deliveryDateTime,
                    'OverAllTotal': data.getProductServiceDtlsByIdMain[0].overAllTotal,
                    'DiscountFromTotal': data.getProductServiceDtlsByIdMain[0].discountFromTotal,
                    'FinalInvAmt': data.getProductServiceDtlsByIdMain[0].finalInvAmt,
                });
                this.Hidden = data.getProductServiceDtlsByIdMain[0].type == "ProductsOnly" ? true : false
                this.MasterSer.GetEmployeeMaster().subscribe((data1: any) => {
                    if (data1.success) {
                        this.EmployeeList = data1.getEmployeeMaster;
                        this.ServiceDoneByList = this.EmployeeList.filter(f => data.getProductServiceDtlsByIdMain[0].serviceDoneById.includes(f.employeeId));
                        this.IsDisableServiceDoneBy = !this.EmployeeList.some(f => data.getProductServiceDtlsByIdMain[0].serviceDoneById.includes(f.employeeId));

                        this.SalesDoneByList = this.EmployeeList.filter(f => data.getProductServiceDtlsByIdMain[0].salesDoneById.includes(f.employeeId));
                        this.IsDisableSalesDoneBy = !this.EmployeeList.some(f => data.getProductServiceDtlsByIdMain[0].salesDoneById.includes(f.employeeId));

                    }
                })


                this.OnChangeCustomer();
                var k = window.setInterval(() => {
                    if (this.VehicleRegNoLstList.length != 0) {
                        this.ServiceProductForm.controls['VehicleRegNo'].setValue(this.VehicleRegNoLstList.find(f => f.vehicleNo == data.getProductServiceDtlsByIdMain[0].vehicleRegNo));
                        this.OnChangeVehicleTagNo()
                        clearInterval(k);
                    }
                }, 100)

                this.ServiceProductSub = [...data.getProductServiceDtlsByIdSub].map((k) => {
                    return {
                        category: k.category,
                        type: k.type,
                        typeId: k.typeId,
                        typeName: k.typeName,
                        price: k.price,
                        qty: k.qty,
                        amount: k.amount,
                        discount: k.discount.toString(),
                        isAvailGst: k.isAvailGst,
                        gstId: k.gstId,
                        gstName: k.gstName,
                        gstValue: k.gstValue,
                        taxableAmt: k.taxableAmt,
                        total: k.total,
                        freeCarWash: (k.category == "CarWash") && (k.freeWash_Status == "FreeWash") ? true : false,
                        freeBikeWash: (k.category == "BikeWash") && (k.freeWash_Status == "FreeWash") ? true : false
                    }
                });

                this.SumOfPrice = this.ServiceProductSub.reduce((a, b) => a + Number(b.price), 0).toFixed(0);
                this.SumOfAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.amount), 0).toFixed(0);
                this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
                this.SumOfDiscount = this.ServiceProductSub.reduce((a, b) => a + Number(b.discount), 0).toFixed(0);
                this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
                this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);
                this.ServiceEntered = this.ServiceProductSub.some((k) => k.type == "Service")

            }
        })

    }



    async GetCustomerMaster() {
        await this.MasterSer.GetCustomerMaster().toPromise().then((data: any) => {
            if (data.success) {
                this.CustomerList = data.getCustomerMaster;
            }
        })
    }

    GetVehicleTagNoDtls() {

        var k = window.setInterval(() => {
            if (this.ServiceProductForm.controls['Customer'].value != null) {
                this.ServiceProdSer.GetVehicleRegNoDtls(this.ServiceProductForm.controls['Customer'].value['customerId']).subscribe((data: any) => {
                    if (data.success) {
                        this.VehicleRegNoLstList = data.getVehicleRegNoDtls;

                        if (this.VehicleRegNoLstList.length == 1) {
                            this.ServiceProductForm.controls['VehicleRegNo'].setValue(this.VehicleRegNoLstList[0]);
                            this.OnChangeVehicleTagNo();
                        }
                    }
                })
                clearInterval(k);
            }
        }, 100)

    }
    async GetServiceDetails() {
        await this.MasterSer.GetServiceMaster().toPromise().then((data: any) => {
            if (data.success) {
                this.ServiceList = data.getServiceMaster;
            }
        })
    }
    async GetProductMaster() {
        await this.MasterSer.GetProductMaster().toPromise().then((data: any) => {
            if (data.success) {
                this.ProductList = data.getProductMaster;
            }
        })
    }
    async GetGstDetails() {
        await this.MasterSer.GetGstMaster().toPromise().then((data: any) => {
            if (data.success) {
                this.GstList = data.getGSTMaster;
            }
        })
    }

    GetEmployeeMaster() {
        this.MasterSer.GetEmployeeMaster().subscribe((data: any) => {
            if (data.success) {
                this.EmployeeList = data.getEmployeeMaster;
            }
        })
    }

    OnChangeCustomer() {
        if (this.ServiceProductForm.controls['Customer'].value != null && this.ServiceProductForm.controls['Customer'].value != undefined) {
            this.ServiceProductForm.controls['MobileNo'].setValue(this.ServiceProductForm.controls['Customer'].value.mobileNo);
        }
        this.VehicleRegNoLstList = []
        this.ServiceProductForm.controls['VehicleRegNo'].setValue(null);
        this.ServiceProductForm.controls['Make'].setValue('');
        this.ServiceProductForm.controls['Model'].setValue('');

        this.GetVehicleTagNoDtls();
    }

    OnChangeVehicleTagNo() {

        if (this.ServiceProductForm.controls['VehicleRegNo'].invalid) {
            this.ServiceProductForm.controls['Make'].setValue('');
            this.ServiceProductForm.controls['Model'].setValue('');
        }
        else {

            this.ServiceProductForm.controls['Make'].setValue(this.ServiceProductForm.controls['VehicleRegNo'].value['make']);
            this.ServiceProductForm.controls['Model'].setValue(this.ServiceProductForm.controls['VehicleRegNo'].value['model']);
        }
    }

    OnBlueVehicleTagNo() {
        if (this.ServiceProductForm.controls['Customer'].invalid) {
            this.SharedSer.showNotification('top', 'center', 'Please Select Customer', 'info')
            return;
        }
    }

    AddRow(i) {

        if (i != 0) {
            if (this.ServiceProductSub[i - 1].typeId == null || this.ServiceProductSub[i - 1].typeId == '' ||
                this.ServiceProductSub[i - 1].price == null || this.ServiceProductSub[i - 1].price == '' ||
                (this.ServiceProductSub[i - 1].isAvailGst && this.ServiceProductSub[i - 1].gstId == null)) {

                this.SharedSer.showNotification('top', 'center', 'Please fill all the required fields, that is marked with *', 'info')
                return;
            }
        }

        this.ServiceEntered = this.ServiceProductSub.some((k) => k.type == "Service")

        this.ServiceProductSub.push({
            category: '',
            type: (this.Hidden == true) ? "Product" : "Service",
            typeId: null,
            typeName: '',
            price: '',
            qty: 1,
            amount: '',
            discount: '0',
            isAvailGst: true,
            gstId: null,
            gstName: '',
            gstValue: '',
            taxableAmt: '',
            total: '',
            freeCarWash: false,
            freeBikeWash: false
        })

        this.IsDisableSalesDoneBy = !(this.ServiceProductSub.some((k: any) => k.type == 'Product'))
        this.IsDisableServiceDoneBy = !(this.ServiceProductSub.some((k: any) => k.type == 'Service'))

        if (this.IsDisableServiceDoneBy) {
            this.ServiceDoneByList = [];
        }
        if (this.IsDisableSalesDoneBy) {
            this.SalesDoneByList = [];
        }
        this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
    }
    RemoveRow(i) {
        this.ServiceProductSub.splice(i, 1);
        this.SumOfPrice = this.ServiceProductSub.reduce((a, b) => a + Number(b.price), 0).toFixed(0);
        this.SumOfAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.amount), 0).toFixed(0);
        this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
        this.SumOfDiscount = this.ServiceProductSub.reduce((a, b) => a + Number(b.discount), 0).toFixed(0);
        this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
        this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);

        this.ServiceProductForm.controls['OverAllTotal'].setValue(this.SumOfTotal)
        this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));

        const ServiceProductSub_categories = this.ServiceProductSub.map(obj => obj.category);
        var categories = ServiceProductSub_categories.join(',');

        this.ServiceProdSer.EligibleForFreeWash(this.ServiceProductForm.controls['MobileNo'].value, categories).subscribe((data: any) => {
            if (data.success) {
                this.IsEligible = data.content == "" ? false : true;
                this.Content = data.content;
                if ((this.IsEligible) && (this.ServiceProductSub.filter((k) => k.freeCarWash == true).length == 0) && (this.ServiceProductSub[i].category == 'CarWash')) {
                    this.ServiceProductSub[i].freeCarWash = true;
                    this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();

                }
                if ((this.IsEligible) && (this.ServiceProductSub.filter((k) => k.freeBikeWash == true).length == 0) && (this.ServiceProductSub[i].category == 'BikeWash')) {
                    this.ServiceProductSub[i].freeBikeWash = true;
                    this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();
                }
                if (!this.IsEligible) {
                    this.ServiceProductSub[i].discount = '0';
                    this.ServiceProductSub[i].freeCarWash = false;
                    this.ServiceProductSub[i].freeBikeWash = false;
                }
                this.OnChangePrice(i);
            }
            else {
                this.OnChangePrice(i);
            }
        });

        this.ServiceEntered = this.ServiceProductSub.some((k) => k.type == "Service")
    }
    OnChangeType(i) {

        this.ServiceProductSub[i].typeId = null
        this.ServiceProductSub[i].typeName = ''
        this.ServiceProductSub[i].price = ''
        this.ServiceProductSub[i].amount = ''
        this.ServiceProductSub[i].qty = 1
        this.ServiceProductSub[i].discount = '0'
        this.ServiceProductSub[i].gstId = null
        this.ServiceProductSub[i].gstName = ''
        this.ServiceProductSub[i].gstValue = ''
        this.ServiceProductSub[i].taxableAmt = ''
        this.ServiceProductSub[i].total = ''
        this.ServiceProductSub[i].freeCarWash = false
        this.ServiceProductSub[i].freeBikeWash = false
        this.ServiceProductSub[i].category = ''

        this.IsDisableSalesDoneBy = !(this.ServiceProductSub.some((k: any) => k.type == 'Product'))
        this.IsDisableServiceDoneBy = !(this.ServiceProductSub.some((k: any) => k.type == 'Service'))

        if (this.IsDisableServiceDoneBy) {
            this.ServiceDoneByList = [];
        }
        if (this.IsDisableSalesDoneBy) {
            this.SalesDoneByList = [];
        }
        this.SumOfPrice = this.ServiceProductSub.reduce((a, b) => a + Number(b.price), 0).toFixed(0);
        this.SumOfAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.amount), 0).toFixed(0);
        this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
        this.SumOfDiscount = this.ServiceProductSub.reduce((a, b) => a + Number(b.discount), 0).toFixed(0);
        this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
        this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);

        this.ServiceProductForm.controls['OverAllTotal'].setValue(this.SumOfTotal)
        this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));

        const ServiceProductSub_categories = this.ServiceProductSub.map(obj => obj.category);
        var categories = ServiceProductSub_categories.join(',');

        this.ServiceProdSer.EligibleForFreeWash(this.ServiceProductForm.controls['MobileNo'].value, categories).subscribe((data: any) => {
            if (data.success) {
                this.IsEligible = data.content == "" ? false : true;
                this.Content = data.content;
                if ((this.IsEligible) && (this.ServiceProductSub.filter((k) => k.freeCarWash == true).length == 0) && (this.ServiceProductSub[i].category == 'CarWash')) {
                    this.ServiceProductSub[i].freeCarWash = true;
                    this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();

                }
                if ((this.IsEligible) && (this.ServiceProductSub.filter((k) => k.freeBikeWash == true).length == 0) && (this.ServiceProductSub[i].category == 'BikeWash')) {
                    this.ServiceProductSub[i].freeBikeWash = true;
                    this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();
                }
                if (!this.IsEligible) {
                    this.ServiceProductSub[i].discount = '0';
                    this.ServiceProductSub[i].freeCarWash = false;
                    this.ServiceProductSub[i].freeBikeWash = false;
                }
                this.OnChangePrice(i);
            }
            else {
                this.OnChangePrice(i);
            }
        });
        this.ServiceEntered = this.ServiceProductSub.some((k) => k.type == "Service");
    }

    async OnChangeServiceProduct(i, Event, Type) {
        if (Event != undefined) {

            const isExists = this.ServiceProductSub.some(({ type, typeId }, index) => index != i && type === this.ServiceProductSub[i].type && typeId === this.ServiceProductSub[i].typeId);
            if (isExists) {
                this.SharedSer.showNotification('top', 'center', this.ServiceProductSub[i].type + " Type Already Exists", "info");
                var k = window.setInterval(() => {
                    if (isExists) {
                        this.ServiceProductSub[i].typeId = null;
                        clearInterval(k);
                    }
                }, 100)
                return;
            }

            this.ServiceProductSub[i].typeName = (Type == 'Service' ? Event.serviceType : Event.productName);
            this.ServiceProductSub[i].price = Event.price;
            this.ServiceProductSub[i].amount = Event.price;
            this.ServiceProductSub[i].qty = 1;
            this.ServiceProductSub[i].isAvailGst = Event.gstId == 0 ? false : true
            this.ServiceProductSub[i].gstId = Event.gstId;
            this.ServiceProductSub[i].category = Event.category == undefined ? '' : Event.category;

            this.SumOfPrice = this.ServiceProductSub.reduce((a, b) => a + Number(b.price), 0).toFixed(0);
            this.SumOfAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.amount), 0).toFixed(0);
            this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
            this.SumOfDiscount = this.ServiceProductSub.reduce((a, b) => a + Number(b.discount), 0).toFixed(0);
            this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
            this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);

            this.ServiceProductForm.controls['OverAllTotal'].setValue(this.SumOfTotal)
            this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));

            const ServiceProductSub_categories = this.ServiceProductSub.map(obj => obj.category);
            var categories = ServiceProductSub_categories.join(',');

            this.ServiceProdSer.EligibleForFreeWash(this.ServiceProductForm.controls['MobileNo'].value, categories).subscribe((data: any) => {
                if (data.success) {
                    this.IsEligible = data.content == "" ? false : true;
                    this.Content = data.content;

                    if ((this.IsEligible) && (this.ServiceProductSub[i].category == 'CarWash')) {
                        this.ServiceProductSub[i].freeCarWash = true;
                        this.ServiceProductSub[i].discount = (Number(this.ServiceProductSub[i].amount).toFixed(0)).toString();

                    }
                    if ((this.IsEligible) && (this.ServiceProductSub[i].category == 'BikeWash')) {
                        this.ServiceProductSub[i].freeBikeWash = true;
                        this.ServiceProductSub[i].discount = (Number(this.ServiceProductSub[i].amount).toFixed(0)).toString();
                    }
                    if (!this.IsEligible) {
                        this.ServiceProductSub[i].discount = '0';
                        this.ServiceProductSub[i].freeCarWash = false;
                        this.ServiceProductSub[i].freeBikeWash = false;
                    }
                    this.OnChangePrice(i);
                }
                else {
                    this.OnChangePrice(i);
                }
            });
        }
        else {

            this.ServiceProductSub[i].category = '';
            this.ServiceProductSub[i].typeName = '';
            this.ServiceProductSub[i].price = '';
            this.ServiceProductSub[i].amount = '';
            this.ServiceProductSub[i].gstId = null
            this.ServiceProductSub[i].gstName = ''
            this.ServiceProductSub[i].discount = '0'
            this.ServiceProductSub[i].gstValue = ''
            this.ServiceProductSub[i].taxableAmt = ''
            this.ServiceProductSub[i].total = ''

            this.SumOfPrice = this.ServiceProductSub.reduce((a, b) => a + Number(b.price), 0).toFixed(0);
            this.SumOfAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.amount), 0).toFixed(0);
            this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
            this.SumOfDiscount = this.ServiceProductSub.reduce((a, b) => a + Number(b.discount), 0).toFixed(0);
            this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
            this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);

            this.ServiceProductForm.controls['OverAllTotal'].setValue(this.SumOfTotal)
            this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));

            const ServiceProductSub_categories = this.ServiceProductSub.map(obj => obj.category);
            var categories = ServiceProductSub_categories.join(',');

            this.ServiceProdSer.EligibleForFreeWash(this.ServiceProductForm.controls['MobileNo'].value, categories).subscribe((data: any) => {
                if (data.success) {
                    this.IsEligible = data.content == "" ? false : true;
                    this.Content = data.content;
                    if ((this.IsEligible) && (this.ServiceProductSub.filter((k) => k.freeCarWash == true).length == 0) && (this.ServiceProductSub[i].category == 'CarWash')) {
                        this.ServiceProductSub[i].freeCarWash = true;
                        this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();

                    }
                    if ((this.IsEligible) && (this.ServiceProductSub.filter((k) => k.freeBikeWash == true).length == 0) && (this.ServiceProductSub[i].category == 'BikeWash')) {
                        this.ServiceProductSub[i].freeBikeWash = true;
                        this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();
                    }
                    if (!this.IsEligible) {
                        this.ServiceProductSub[i].discount = '0';
                        this.ServiceProductSub[i].freeCarWash = false;
                        this.ServiceProductSub[i].freeBikeWash = false;
                    }
                    this.OnChangePrice(i);
                }
                else {
                    this.OnChangePrice(i);
                }
            });

        }

    }

    OnChangePrice(i) {
        this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].discount).toString();
        if (this.ServiceProductSub[i].price != '') {
            if (Number(this.ServiceProductSub[i].price) < 0) {
                this.ServiceProductSub[i].price = 0;
                this.ServiceProductSub[i].amount = 0;
                this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Price', 'info')
            }
            this.ServiceProductSub[i].amount = Number(Number(this.ServiceProductSub[i].price) * Number(this.ServiceProductSub[i].qty)).toFixed(0)
            if (this.IsEligible || (this.ServiceProductId != 0 && (this.ServiceProductSub[i].freeCarWash || this.ServiceProductSub[i].freeBikeWash))) {
                this.ServiceProductSub[i].discount = (this.ServiceProductSub[i].amount).toString();
            }
            this.ServiceProductSub[i].taxableAmt = Number(((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) * Number(this.ServiceProductSub[i].gstValue || 0)) / 100).toFixed(0);
            if (Number(this.ServiceProductSub[i].discount) < 0 || Number(this.ServiceProductSub[i].discount) > Number(this.ServiceProductSub[i].amount)) {
                this.ServiceProductSub[i].discount = '0';
                this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Discount Amount', 'info')
            }
            this.OnChangeGst(i, this.GstList.find(f => f.gstId == this.ServiceProductSub[i].gstId))
        }
    }

    OnChangeGst(i, Event) {
        if (Event != undefined) {
            this.ServiceProductSub[i].gstName = Event.gstName
            this.ServiceProductSub[i].gstValue = Event.gstValue
            this.ServiceProductSub[i].taxableAmt = Number(((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) * Number(this.ServiceProductSub[i].gstValue || 0)) / 100).toFixed(0);
            this.ServiceProductSub[i].total = Number((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) + Number(this.ServiceProductSub[i].taxableAmt)).toFixed(0);
        }
        else {
            this.ServiceProductSub[i].gstName = ''
            this.ServiceProductSub[i].gstValue = ''
            this.ServiceProductSub[i].taxableAmt = Number(((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) * Number(this.ServiceProductSub[i].gstValue || 0)) / 100).toFixed(0);
            this.ServiceProductSub[i].total = Number((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) + Number(this.ServiceProductSub[i].taxableAmt)).toFixed(0);
        }

        this.SumOfPrice = this.ServiceProductSub.reduce((a, b) => a + Number(b.price), 0).toFixed(0);
        this.SumOfAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.amount), 0).toFixed(0);
        this.SumOfQty = this.ServiceProductSub.reduce((a, b) => a + Number(b.qty), 0).toFixed(0);
        this.SumOfDiscount = this.ServiceProductSub.reduce((a, b) => a + Number(b.discount), 0).toFixed(0);
        this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
        this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);

        this.ServiceProductForm.controls['OverAllTotal'].setValue(this.SumOfTotal)
        this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));

    }

    OnChangeIsAvilGst(i) {

        if (this.ServiceProductSub[i].isAvailGst == false) {
            this.ServiceProductSub[i].gstName = '';
            this.ServiceProductSub[i].gstValue = '';
            this.ServiceProductSub[i].gstId = 0;
            this.ServiceProductSub[i].taxableAmt = Number(((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) * Number(this.ServiceProductSub[i].gstValue || 0)) / 100).toFixed(0);
            this.ServiceProductSub[i].total = Number((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) + Number(this.ServiceProductSub[i].taxableAmt)).toFixed(0);
        }
        else {
            this.ServiceProductSub[i].gstId = null;
            this.ServiceProductSub[i].taxableAmt = Number(((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) * Number(this.ServiceProductSub[i].gstValue || 0)) / 100).toFixed(0);
            this.ServiceProductSub[i].total = Number((Number(this.ServiceProductSub[i].amount || 0) - Number(this.ServiceProductSub[i].discount || 0)) + Number(this.ServiceProductSub[i].taxableAmt)).toFixed(0);
        }
        this.SumOfTaxableAmt = this.ServiceProductSub.reduce((a, b) => a + Number(b.taxableAmt), 0).toFixed(0);
        this.SumOfTotal = this.ServiceProductSub.reduce((a, b) => a + Number(b.total), 0).toFixed(0);
        this.ServiceProductForm.controls['OverAllTotal'].setValue(this.SumOfTotal);
        this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));
    }

    OnChangeDiscountFromTotal() {
        if (Number(this.ServiceProductForm.controls['DiscountFromTotal'].value) < 0 ||
            Number(this.ServiceProductForm.controls['DiscountFromTotal'].value) > Number(this.SumOfTotal)) {
            this.SharedSer.showNotification('top', 'center', 'Please Enter Valid Discount Amount', 'info');
            this.ServiceProductForm.controls['DiscountFromTotal'].setValue(0);
            this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0)))
            return;
        }
        else {
            this.ServiceProductForm.controls['FinalInvAmt'].setValue((Number(this.SumOfTotal || 0) - Number(this.ServiceProductForm.controls['DiscountFromTotal'].value)).toFixed(0));
        }

    }

    Save(Key: any) {
        this.Submitted = true;
        if (this.ServiceProductForm.invalid || (!this.IsDisableServiceDoneBy && this.ServiceDoneByList.length == 0)
            || (!this.IsDisableSalesDoneBy && this.SalesDoneByList.length == 0)) {
            this.SharedSer.showNotification('top', 'center', 'Please fill all the required fields, that is marked with *', 'info')
            return;
        }

        if (this.ServiceProductSub.find(f => f.typeId == '' || f.typeId == null || f.price == '' || f.price == null ||
            (f.isAvailGst && f.gstId == null))) {
            this.SharedSer.showNotification('top', 'center', 'Please fill all the required fields, that is marked with *', 'info')
            return;
        }
        if (this.ServiceProductForm.controls["Type"].value == "ServiceAndProducts" &&
            this.ServiceProductSub.filter((k) => k.type == "Service").length == 0) {
            this.SharedSer.showNotification('top', 'center', 'Please Add Service, Otherwise Click Products only button', 'info')
            return;
        }
        let ServiceDoneByIds = [];
        this.ServiceDoneByList.forEach((element) => {
            ServiceDoneByIds.push(element.employeeId);
        });
        let ServiceDoneByNames = [];
        this.ServiceDoneByList.forEach((element) => {
            ServiceDoneByNames.push(element.employeeName);
        });
        let SalesDoneByIds = [];
        this.SalesDoneByList.forEach((element) => {
            SalesDoneByIds.push(element.employeeId);
        });
        let SalesDoneByNames = [];
        this.SalesDoneByList.forEach((element) => {
            SalesDoneByNames.push(element.employeeName);
        });

        var ServiceProductSub = this.ServiceProductSub.map((k) => {
            return {
                category: k.category,
                type: k.type,
                typeId: k.typeId,
                typeName: k.typeName,
                price: k.price.toString(),
                qty: k.qty,
                amount: k.amount,
                discount: k.discount.toString(),
                isAvailGst: k.isAvailGst,
                gstId: k.gstId,
                gstName: k.gstName,
                gstValue: k.gstValue.toString(),
                taxableAmt: k.taxableAmt.toString(),
                total: k.total.toString(),
                freeCarWash: k.freeCarWash,
                freeBikeWash: k.freeBikeWash
            }
        })
        let inputObj = {
            service_ProductId: Number(this.ServiceProductId),
            service_ProductNo: this.ServiceProductForm.controls['ServiceNo'].value == null || undefined ? "" : this.ServiceProductForm.controls['ServiceNo'].value,
            type: this.ServiceProductForm.controls['Type'].value,
            date: this.ServiceProductForm.controls['Date'].value,
            mobileNo: (this.ServiceProductForm.controls['MobileNo'].value).toString(),
            customerId: this.ServiceProductForm.controls['Customer'].value['customerId'],
            customerName: this.ServiceProductForm.controls['Customer'].value['customerName'],
            vehicleRegNo: this.ServiceProductForm.controls['Type'].value == "ProductsOnly" ? "" : this.ServiceProductForm.controls['VehicleRegNo'].value['vehicleNo'],
            makeId: this.ServiceProductForm.controls['Type'].value == "ProductsOnly" ? 0 : this.ServiceProductForm.controls['VehicleRegNo'].value['makeId'],
            make: this.ServiceProductForm.controls['Type'].value == "ProductsOnly" ? "" : this.ServiceProductForm.controls['Make'].value,
            modelId: this.ServiceProductForm.controls['Type'].value == "ProductsOnly" ? 0 : this.ServiceProductForm.controls['VehicleRegNo'].value['modelId'],
            model: this.ServiceProductForm.controls['Type'].value == "ProductsOnly" ? "" : this.ServiceProductForm.controls['Model'].value,
            deliveryDateTime: this.ServiceProductForm.controls['DeliveryDateTime'].value,
            serviceDoneById: this.IsDisableServiceDoneBy ? '' : ServiceDoneByIds.join(','),
            serviceDoneBy: this.IsDisableServiceDoneBy ? '' : ServiceDoneByNames.join(','),
            salesDoneById: this.IsDisableSalesDoneBy ? '' : SalesDoneByIds.join(','),
            salesDoneBy: this.IsDisableSalesDoneBy ? '' : SalesDoneByNames.join(','),
            overAllTax: this.SumOfTaxableAmt,
            overallTotal: this.ServiceProductForm.controls['OverAllTotal'].value,
            discFromTotal: this.ServiceProductForm.controls['DiscountFromTotal'].value == null || undefined ? "" : (this.ServiceProductForm.controls['DiscountFromTotal'].value).toString(),
            finalAmount: this.ServiceProductForm.controls['FinalInvAmt'].value,
            createdBy: Number(localStorage.getItem("userId")),
            serviceProductSub: ServiceProductSub
        }
        this.SpinnerSer.show();
        console.log(inputObj);
        this.ServiceProdSer.InsertServiceProduct(inputObj).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', "Product/Service Details" + (this.ServiceProductId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
                if (Key == 'Print') {
                    this.MasterSer.GetCompanyMaster().subscribe((data1: any) => {
                        if (data1.success) {
                            if (data1.getCompanyMaster.length == 0) {
                                this.SharedSer.showNotification('top', 'center', 'Please Add Company Details', 'info')
                                return;
                            }
                            else {
                                this.Print(data.service_ProductId);
                            }
                        }
                    })
                }
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.SpinnerSer.hide();
            this.router.navigate(['/service/view-service'])
        })
    }

    Print(Service_ProductId) {
        this.SpinnerSer.show();
        this.ServiceProdSer.PrintInvoice(Service_ProductId).subscribe((data: any) => {
            let blob: any = new Blob([data], { type: 'application/pdf' });
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
                var base64data = reader.result;
                printJS({ printable: base64data.toString().replace('data:application/pdf;base64,', ''), type: 'pdf', base64: true, showModal: true })
            }
            this.SpinnerSer.hide();
        })
    }

}
