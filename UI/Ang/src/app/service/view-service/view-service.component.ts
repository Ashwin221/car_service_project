import { Component, OnInit } from '@angular/core';
import { MasterService } from 'app/Services/master.service';
import { ServiceproductService } from 'app/Services/serviceproduct.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as printJS from 'print-js';

declare var $: any;
@Component({
  selector: 'app-view-service',
  templateUrl: './view-service.component.html',
  styleUrls: ['./view-service.component.css']
})
export class ViewServiceComponent implements OnInit {
  ServiceProductList: any = [];
  Visible: boolean = false;
  ServiceProductId: any = 0;
  Submitted: boolean = false;
  Remarks: any = '';
  menuList: any = [];
  LoginUserId: any = 0;
  constructor(private SerProdSer: ServiceproductService, private SpinnerSer: NgxSpinnerService,
    private SharedSer: SharedService, private ServiceProdSer: ServiceproductService, private MasterSer: MasterService) { }

  ngOnInit(): void {
    this.GetProductServiceDtls();
    if (localStorage.getItem('LoginUserDtls') != null) {
      var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
      this.menuList = LoginUserDtls[0].allocateMenu.split(',');
      if (localStorage.getItem('UserType') == "User") {
        this.LoginUserId = localStorage.getItem('userId');
      }
    }
  }
  GetProductServiceDtls() {
    this.Visible = false;
    this.SpinnerSer.show();
    this.SerProdSer.GetProductServiceDtls().subscribe((data: any) => {
      if (data.success) {
        this.ServiceProductList = data.getProductServiceDtls;
      }
      this.Visible = true;
      this.SpinnerSer.hide();
    })
  }


  Delete(ServiceProductId, Key) {
    if (Key == 'Assign') {
      this.Remarks = "";
      this.ServiceProductId = ServiceProductId;
      this.Submitted = false;
      $("#InActiveServiceProductModal").modal('show');
      return;
    }

    this.Submitted = true;
    if (this.Remarks == '' || this.Remarks.trim() == '') {
      return
    }
    this.SpinnerSer.show();
    this.ServiceProdSer.DeleteProductServiceMaster(this.ServiceProductId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
      if (data.success) {
        this.SharedSer.showNotification('top', 'center', 'Service/Product Details Deleted Successfully', 'danger');
      }
      else {
        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
      }
      this.SpinnerSer.hide();
      this.GetProductServiceDtls();
      document.getElementById('closeInActiveServiceProductModal').click();
    })
  }

  Print(Service_ProductId) {
    this.MasterSer.GetCompanyMaster().subscribe(async (data: any) => {
      if (data.success) {
        if (data.getCompanyMaster.length == 0) {
          this.SharedSer.showNotification('top', 'center', 'Please Add Company Details', 'info')
          return;
        }
        else {
          this.SpinnerSer.show();
          await this.ServiceProdSer.PrintInvoice(Service_ProductId).subscribe((data: any) => {
            let blob: any = new Blob([data], { type: 'application/pdf' });
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
              var base64data = reader.result;
              printJS({ printable: base64data.toString().replace('data:application/pdf;base64,', ''), type: 'pdf', base64: true, showModal: true })
            }
            this.SpinnerSer.hide();
          })
        }
      }
    })

  }
}
