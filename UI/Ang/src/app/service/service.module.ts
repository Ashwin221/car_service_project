import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRoutingModule } from './service-routing.module';
import { CreateServiceComponent } from './create-service/create-service.component';
import { ViewServiceComponent } from './view-service/view-service.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


@NgModule({
  declarations: [CreateServiceComponent, ViewServiceComponent],
  imports: [
    CommonModule,
    ServiceRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    JwBootstrapSwitchNg2Module,
    NgMultiSelectDropDownModule
  ]
})
export class ServiceModule { }
