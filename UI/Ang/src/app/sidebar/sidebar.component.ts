import { Component, OnInit, AfterViewInit, AfterViewChecked, AfterContentInit } from '@angular/core';
import { MasterService } from 'app/Services/master.service';
import { Router } from '@angular/router';

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    collapse?: string;
    icontype: string;
    status: string[];
    // icon: string;
    children?: ChildrenItems[];
    menuId?: any;
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
    menuId?: any;
    status: string[];
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    {
        path: '/master',
        title: 'Master',
        type: 'sub',
        status: ['Admin', 'User'],
        collapse: 'Master',
        menuId: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40'],
        icontype: 'nc-icon nc-bullet-list-67',
        children: [
            {
                path: 'unit-master', title: 'Unit Master', ab: 'UM', menuId: ['1', '2', '3', '4'],
                status: ['Admin', 'User']
            },
            {
                path: 'gst-master', title: 'GST Master', ab: 'GM', menuId: ['5', '6', '7', '8'],
                status: ['Admin', 'User']
            },
            {
                path: 'view-product-master', title: 'Product Master', ab: 'IM', menuId: ['9', '10', '11', '12'],
                status: ['Admin', 'User']
            },
            {
                path: 'employee-master', title: 'Employee Master', ab: 'EM', menuId: ['13', '14', '15', '16'],
                status: ['Admin', 'User']
            },
            {
                path: 'service-master', title: 'Service Master', ab: 'SM', menuId: ['17', '18', '19', '20'],
                status: ['Admin', 'User']
            },
            {
                path: 'view-company-master', title: 'Company Master', ab: 'CM', menuId: ['21', '22', '23', '24'],
                status: ['Admin', 'User']
            },
            {
                path: 'view-user-master', title: 'User Master', ab: 'UM', menuId: ['25', '26', '27', '28'],
                status: ['Admin', 'User']
            },
            {
                path: 'make-master', title: 'Make Master', ab: 'MM', menuId: ['29', '30', '31', '32'],
                status: ['Admin', 'User']
            },
            {
                path: 'model-master', title: 'Model Master', ab: 'MM', menuId: ['33', '34', '35', '36'],
                status: ['Admin', 'User']
            },
            {
                path: 'payment-mode-master', title: 'Payment Mode Master', ab: 'PMM', menuId: ['37', '38', '39', '40'],
                status: ['Admin', 'User']
            },


        ]
    },
    {
        path: '/customer/view-customer-master',
        title: 'Customer',
        type: 'link',
        collapse: 'Task',
        status: ['Admin', 'User'],
        menuId: ['41', '42', '43', '44'],
        icontype: 'fa fa-users'

    },
    {
        path: '/service/view-service',
        title: 'Service / Product',
        type: 'link',
        collapse: 'Task',
        status: ['Admin', 'User'],
        menuId: ['45', '46', '47', '48'],
        icontype: 'fa fa-cog'

    },

    {
        path: '/reports',
        title: 'Reports',
        type: 'sub',
        status: ['Admin', 'User'],
        collapse: 'TaskReport',
        menuId: ['49', '50', '51', '52'],
        icontype: 'fa fa-file',
        children: [
            { path: 'total-services', title: 'Total Services', ab: 'TS', menuId: ['49'], status: ['Admin', 'User'] },
            { path: 'total-product-sales', title: 'Total Product Sales', ab: 'TPS', menuId: ['50'], status: ['Admin', 'User'] },
            { path: 'employee-wise-service', title: 'Employee Wise Service', ab: 'EWS', menuId: ['51'], status: ['Admin', 'User'] },
            { path: 'employee-wise-product-sales', title: 'Employee Wise Product Sales', ab: 'EPS', menuId: ['52'], status: ['Admin', 'User'] },
        ]
    },
];

@Component({
    // moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent {
    customerId: any;
    Name: any;
    userId: any;
    ImgPath: any;
    PersonalList: any = [];
    ProfileVisible: boolean = true;
    imgUrl;
    public menuItems: any[];
    menuList: any = [];
    profileImage: any;
    imgUrl1: any;
    username: any;
    userType: any;


    constructor(private services: MasterService, private router: Router) { }
    isNotMobileMenu() {
        if (window.outerWidth > 991) {
            return false;
        }
        return true;
    }

    ngOnInit() {
        //this.profileImage = localStorage.getItem('profileImage');

        // if (this.profileImage != '' ) {
        //     this.getImageFromService();
        // }
        this.username = localStorage.getItem('UserName')
        this.userType = localStorage.getItem('UserType')

        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (localStorage.getItem('LoginUserDtls') != null) {
            this.customerId = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.userId = this.customerId[0].userId;
            this.Name = this.customerId[0].userName;
            this.menuList = this.customerId[0].allocateMenu.split(',');
        }
    }

    View() {
        var id = localStorage.getItem('EmpId');
        this.router.navigate(['master', 'createemployee'], { queryParams: { 'view': id } })
    }

    Change() {
        this.router.navigate(['settings', 'changePwd'])
    }

    getImageFromService() {
        // this.services.GetFile(this.profileImage).subscribe(data => {
        //     this.createImageFromBlob(data);
        // }, error => {
        //     //this.logoPath = "./assets/img/placeholder.jpg";
        //     console.log(error);
        // });
    }
    createImageFromBlob(image: Blob) {
        //alert('sdsd')
        let reader = new FileReader();
        reader.addEventListener("load", () => {
            //alert(reader.result)
            this.imgUrl1 = reader.result;

        }, false);

        if (image) {
            reader.readAsDataURL(image);
        }
    }


    ngAfterViewInit() {
    }



    ShowMainMenu(array2: any) {
        return array2.some(element => this.menuList.includes(element));
    }


}


