import { Component, OnInit } from '@angular/core';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare var $: any;
@Component({
    selector: 'app-view-product-master',
    templateUrl: './view-product-master.component.html',
    styleUrls: ['./view-product-master.component.css']
})
export class ViewProductMasterComponent implements OnInit {

    Visible: boolean = false;
    ProductList: any = [];
    Submitted: boolean = false;
    Remarks: any = '';
    ProductId: any = 0;
    LoginUserId: any = 0;
    menuList: any = [];
    constructor(private MasterSer: MasterService, private SharedSer: SharedService, private spinnerServ: NgxSpinnerService) { }

    ngOnInit(): void {
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
        this.GetProductMaster();
    }
    GetProductMaster() {
        this.Visible = false;
        this.spinnerServ.show();
        this.MasterSer.GetProductMaster().subscribe((data: any) => {
            if (data.success) {
                this.ProductList = data.getProductMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    Delete(ProductId, Key) {
        $('#InActiveProductModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        if (Key == 'Assign') {
            this.ProductId = ProductId;
            this.Submitted = false;
            $("#InActiveProductModal").modal('show');
            this.Remarks = "";
            return;
        }

        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeleteProductMaster(this.ProductId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Product Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetProductMaster();
            document.getElementById('closeInActiveProductModal').click();
        })
    }
}
