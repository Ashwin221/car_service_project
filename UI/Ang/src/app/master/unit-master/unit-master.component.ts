import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
    selector: 'app-unit-master',
    templateUrl: './unit-master.component.html',
    styleUrls: ['./unit-master.component.css']
})
export class UnitMasterComponent implements OnInit {
    UnitId: any = 0;
    UnitList: any = [];
    UnitForm: FormGroup;
    Submitted: boolean = false;
    LoginUserId: any = 0;
    Visible: boolean = false;
    Remarks: any = '';
    UnitCodeExists: boolean = false;
    UnitNameExists: boolean = false;
    menuList: any = [];
    constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService, private spinnerServ: NgxSpinnerService) {
        this.UnitForm = this.fb.group({
            unitName: ['', Validators.required],
            unitCode: ['', Validators.required]
        })
    }

    ngOnInit(): void {
        this.GetUnitDetails();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    OnChangeUnit(Key: any) {
        if (Key == "UnitCode") {
            this.UnitCodeExists = this.UnitList.some((i: any) => i.unitCode.toLowerCase() == this.UnitForm.controls['unitCode'].value.trim().toLowerCase() && i.unitId != this.UnitId);
        }
        if (Key == "UnitName") {
            this.UnitNameExists = this.UnitList.some((i: any) => i.unitName.toLowerCase() == this.UnitForm.controls['unitName'].value.trim().toLowerCase() && i.unitId != this.UnitId);
        }
    }

    GetUnitDetails() {
        this.Visible = false;
        this.UnitList = [];
        this.spinnerServ.show();
        this.MasterSer.GetUnitMaster().subscribe((data: any) => {
            if (data.success) {
                this.UnitList = data.getUnitMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    AddNew(key, Unitid) {
        $('#AddUnitModal').on('shown.bs.modal', function () {
            $('#add').focus()
        })
        $('#InActiveUnitModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        this.UnitId = Unitid;
        this.Remarks = '';
        this.UnitCodeExists = false;
        this.UnitNameExists = false;
        this.UnitForm.reset();
        this.Submitted = false;

        if (key == 'Edit') {
            this.MasterSer.GetUnitDetailsById(this.UnitId).subscribe((data: any) => {
                if (data.success) {
                    this.UnitForm.patchValue({
                        unitName: data.getUnitMasterById[0].unitName,
                        unitCode: data.getUnitMasterById[0].unitCode,
                    })
                    this.UnitId = data.getUnitMasterById[0].unitId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.UnitForm.invalid || this.UnitCodeExists || this.UnitNameExists) {
            return;
        }
        let inputObj = {
            unitId: this.UnitId,
            unitCode: this.UnitForm.controls['unitCode'].value.trim(),
            unitName: this.UnitForm.controls['unitName'].value.trim(),
            createdBy: this.LoginUserId
        }
        console.log(inputObj)
        this.spinnerServ.show();
        this.MasterSer.InsertUnitMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Unit Details" + (this.UnitId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.spinnerServ.hide();
            this.GetUnitDetails();
            document.getElementById('closeAddUnitModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeleteUnitMaster(this.UnitId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Unit Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetUnitDetails();
            document.getElementById('closeInActiveUnitModal').click();
        })
    }

}
