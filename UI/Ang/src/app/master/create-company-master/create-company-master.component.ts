import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';

@Component({
  selector: 'app-create-company-master',
  templateUrl: './create-company-master.component.html',
  styleUrls: ['./create-company-master.component.css']
})
export class CreateCompanyMasterComponent implements OnInit {
  CompanyForm: FormGroup;
  CompanyId: any = 0;
  StateList: any = [];
  CityList: any = [];
  Submitted: boolean = false;
  CompanyList: any = [];
  CompanyNameExists: boolean = false;
  CompanyDtls: any = [];
  menuList: any = [];
  LoginUserId: any;

  constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService,
    private router: Router, private activatedRoute: ActivatedRoute) {
    this.CompanyForm = this.fb.group({
      companyName: ['', Validators.required],
      state: [null, Validators.required],
      city: [null, Validators.required],
      pincode: ['', Validators.required],
      address1: ['', Validators.required],
      address2: [''],
      phoneNo: ['', Validators.required],
      email: ['', Validators.required],
      website: [''],
    })
  }

  ngOnInit(): void {
    document.getElementById("add").focus();
    if (localStorage.getItem('LoginUserDtls') != null) {
      var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
      this.menuList = LoginUserDtls[0].allocateMenu.split(',');
      if (localStorage.getItem('UserType') == "User") {
        this.LoginUserId = localStorage.getItem('userId');
      }
    }
    this.GetStateMaster();
    if (this.activatedRoute.snapshot.queryParamMap.get("CompanyId") != null) {
      this.CompanyId = this.activatedRoute.snapshot.queryParamMap.get("CompanyId");
      this.GetCompanyMasterById();
    }
    this.GetCompanyMaster();
  }


  GetCompanyMaster() {
    this.CompanyDtls = [];
    this.MasterSer.GetCompanyMaster().subscribe((data: any) => {
      if (data.success) {
        this.CompanyDtls = data.getCompanyMaster;
      }
    })
  }

  OnChangeCompanyName() {
    this.CompanyNameExists = this.CompanyDtls.some((i: any) => i.companyName.toLowerCase() == this.CompanyForm.controls['companyName'].value.trim().toLowerCase() && i.companyId != this.CompanyId);
  }

  GetCompanyMasterById() {
    this.MasterSer.GetCompanyMasterById(this.CompanyId).subscribe((data: any) => {
      if (data.success) {
        this.CompanyList = data.getCompanyMasterById;
        this.CompanyForm.patchValue({
          companyName: this.CompanyList[0].companyName,
          state: this.CompanyList[0].stateId,
          pincode: this.CompanyList[0].pincode,
          address1: this.CompanyList[0].address1,
          address2: this.CompanyList[0].address2,
          phoneNo: this.CompanyList[0].mobileNo,
          email: this.CompanyList[0].email,
          website: this.CompanyList[0].website,
        })
      }
      this.OnChangeState();
      this.CompanyForm.controls['city'].setValue(this.CompanyList[0].cityId);
    })
  }

  GetStateMaster() {
    this.MasterSer.GetStateMaster().subscribe((data: any) => {
      if (data.success) {
        this.StateList = data.getStateMaster;
      }
    })
  }
  OnChangeState() {
    this.MasterSer.GetDistrictMasterByStateId(this.CompanyForm.controls['state'].value).subscribe((data: any) => {
      if (data.success) {
        this.CityList = data.getDistrictMasterByStateId;
      }
    })
    this.CompanyForm.controls['city'].setValue(null);
  }

  Save() {
    this.Submitted = true;
    if (this.CompanyForm.invalid || this.CompanyNameExists) {
      return;
    }
    let inputObj = {
      companyId: Number(this.CompanyId),
      companyName: this.CompanyForm.controls['companyName'].value,
      mobileNo: this.CompanyForm.controls['phoneNo'].value.toString(),
      email: this.CompanyForm.controls['email'].value,
      website: this.CompanyForm.controls['website'].value,
      stateId: this.CompanyForm.controls['state'].value,
      cityId: this.CompanyForm.controls['city'].value,
      pincode: this.CompanyForm.controls['pincode'].value.toString(),
      address1: this.CompanyForm.controls['address1'].value,
      address2: this.CompanyForm.controls['address2'].value,
      createdBy: Number(localStorage.getItem("userId"))
    }   
    console.log(inputObj);
     
    this.MasterSer.InsertCompanyMaster(inputObj).subscribe((data: any) => {
      if (data.success) {
        this.SharedSer.showNotification("top", "center", "Comapny Details Saved Succesfully", "success")
        this.router.navigate(["master", "view-company-master"])
      }
    })
  }
}
