import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-make-master',
    templateUrl: './make-master.component.html',
    styleUrls: ['./make-master.component.css']
})
export class MakeMasterComponent implements OnInit {
    MakeId: any = 0;
    MakeList: any = [];
    MakeForm: FormGroup;
    Submitted: boolean = false;
    LoginUserId: any = 0;
    Visible: boolean = false;
    Remarks: any = '';
    menuList: any = [];
    MakeNameExists: boolean = false;

    constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService, private spinnerServ: NgxSpinnerService) {
        this.MakeForm = this.fb.group({
            MakeName: ['', Validators.required]
        })
    }

    ngOnInit(): void {
        this.GetMakeDetails();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    GetMakeDetails() {
        this.Visible = false;
        this.spinnerServ.show();
        this.MasterSer.GetMakeMaster().subscribe((data: any) => {
            if (data.success) {
                this.MakeList = data.getMakeMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    OnChangeMake() {
        this.MakeNameExists = this.MakeList.some((i: any) => i.make.toLowerCase() == this.MakeForm.controls['MakeName'].value.trim().toLowerCase() && i.makeId != this.MakeId);
    }

    AddNew(key, Makeid) {
        $('#AddMakeModal').on('shown.bs.modal', function () {
            $('#add').focus()
        })
        $('#InActiveMakeModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        this.MakeId = Makeid;
        this.MakeNameExists = false;
        this.Remarks = '';
        this.MakeForm.reset();
        this.Submitted = false;
        if (key == 'Edit') {
            this.MasterSer.GetMakeDetailsById(this.MakeId).subscribe((data: any) => {
                if (data.success) {
                    this.MakeForm.patchValue({
                        MakeName: data.getMakeMasterById[0].make,
                    })
                    this.MakeId = data.getMakeMasterById[0].makeId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.MakeForm.invalid) {
            return;
        }
        let inputObj = {
            makeId: this.MakeId,
            make: this.MakeForm.controls['MakeName'].value.trim(),
            createdBy: this.LoginUserId
        }
        console.log(inputObj)
        this.spinnerServ.show();
        this.MasterSer.InsertMakeMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Make Details" + (this.MakeId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.spinnerServ.hide();
            this.GetMakeDetails();
            document.getElementById('closeAddMakeModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeleteMakeMaster(this.MakeId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Make Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetMakeDetails();
            document.getElementById('closeInActiveMakeModal').click();
        })
    }


}