import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-create-product-master',
  templateUrl: './create-product-master.component.html',
  styleUrls: ['./create-product-master.component.css']
})
export class CreateProductMasterComponent implements OnInit {
  UnitList: any = [];
  GSTList: any = [];
  ProductForm: FormGroup;
  ProductId: any = 0;
  Submitted: boolean = false;
  LoginUserId: any = 0;
  ProductList: any = [];
  ProductCodeExists: boolean = false;
  ProductNameExists: boolean = false;
  menuList: any = [];

  constructor(private fb: FormBuilder, private MasterSer: MasterService, private spinnerServ: NgxSpinnerService,
    private SharedSer: SharedService, private router: Router, private activateRoute: ActivatedRoute) {
    this.ProductForm = this.fb.group({
      ProductCode: ['', Validators.required],
      ProductName: ['', Validators.required],
      description: ['', Validators.required],
      unit: [null, Validators.required],
      price: ['', Validators.required],
      Gst: [null]
    })
  }

  async ngOnInit() {
    if (localStorage.getItem('LoginUserDtls') != null) {
      var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
      this.menuList = LoginUserDtls[0].allocateMenu.split(',');
      if (localStorage.getItem('UserType') == "User") {
        this.LoginUserId = localStorage.getItem('userId');
      }
    }
    document.getElementById("add").focus();
    await this.GetUnitDetails();
    await this.GetGstDetails();
    if (this.activateRoute.snapshot.queryParamMap.get('ProductId') != null) {
      this.ProductId = this.activateRoute.snapshot.queryParamMap.get('ProductId');
      this.GetProductMasterById();
    }
    this.GetProductMaster();
  }

  GetProductMaster() {
    this.ProductList = [];
    this.MasterSer.GetProductMaster().subscribe((data: any) => {
      if (data.success) {
        this.ProductList = data.getProductMaster;
      }
    })
  }

  OnChangeProduct(Key: any) {
    if (Key == "ProductCode") {
      this.ProductCodeExists = this.ProductList.some((i: any) => i.productCode.toLowerCase() == this.ProductForm.controls['ProductCode'].value.trim().toLowerCase() && i.productId != this.ProductId);
    }
    if (Key == "ProductName") {
      this.ProductNameExists = this.ProductList.some((i: any) => i.productName.toLowerCase() == this.ProductForm.controls['ProductName'].value.trim().toLowerCase() && i.productId != this.ProductId);
    }
  }

  GetProductMasterById() {
    this.MasterSer.GetProductMasterById(this.ProductId).subscribe((data: any) => {
      if (data.success) {
        this.ProductForm.patchValue({
          ProductCode: data.getProductMasterById[0].productCode,
          ProductName: data.getProductMasterById[0].productName,
          description: data.getProductMasterById[0].description,
          unit: this.UnitList.find(f => f.unitId == data.getProductMasterById[0].unitId),
          price: data.getProductMasterById[0].price,
          Gst: this.GSTList.find(f => f.gstId == data.getProductMasterById[0].gstId)
        })
      }
    })
  }

  async GetUnitDetails() {
    await this.MasterSer.GetUnitMaster().toPromise().then((data: any) => {
      if (data.success) {
        this.UnitList = data.getUnitMaster;
      }
    })
  }
  async GetGstDetails() {
    await this.MasterSer.GetGstMaster().toPromise().then((data: any) => {
      if (data.success) {
        this.GSTList = data.getGSTMaster;
      }
    })
  }

  Save() {

    this.Submitted = true;
    if (this.ProductForm.invalid || this.ProductCodeExists || this.ProductNameExists) {
      return;
    }
    let inputObj = {
      productId: Number(this.ProductId),
      productCode: this.ProductForm.controls['ProductCode'].value.trim(),
      productName: this.ProductForm.controls['ProductName'].value.trim(),
      description: this.ProductForm.controls['description'].value.trim(),
      unitId: Number(this.ProductForm.controls['unit'].value['unitId']),
      unitName: this.ProductForm.controls['unit'].value['unitName'].toString(),
      price: this.ProductForm.controls['price'].value.toString(),
      gstId: this.ProductForm.controls['Gst'].value == null ? 0 : Number(this.ProductForm.controls['Gst'].value['gstId']),
      gstName: this.ProductForm.controls['Gst'].value == null ? "" : this.ProductForm.controls['Gst'].value['gstName'].toString(),
      gstValue: this.ProductForm.controls['Gst'].value == null ? "" : this.ProductForm.controls['Gst'].value['gstValue'].toString(),
      createdBy: Number(localStorage.getItem("userId"))
    }    
    console.log(inputObj);
    
    this.spinnerServ.show();
    this.MasterSer.InsertProductMaster(inputObj).subscribe((data: any) => {
      if (data.success) {
        this.SharedSer.showNotification('top', 'center', 'Product Details Saved Successfully', 'success');
      }
      else {
        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
      }
      this.spinnerServ.hide();
      this.router.navigate(['/master/view-product-master'])
    })
  }

}
