import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-payment-mode-master',
    templateUrl: './payment-mode-master.component.html',
    styleUrls: ['./payment-mode-master.component.css']
})
export class PaymentModeMasterComponent implements OnInit {
    PaymentModeId: any = 0;
    PaymentModeList: any = [];
    PaymentModeForm: FormGroup;
    Submitted: boolean = false;
    LoginUserId: any = 0;
    Visible: boolean = false;
    Remarks: any = '';
    menuList: any = [];
    PaymentModeExist: boolean = false;

    constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService, private spinnerServ: NgxSpinnerService) {
        this.PaymentModeForm = this.fb.group({
            PaymentMode: ['', Validators.required]
        })
    }

    ngOnInit(): void {
        this.GetPaymentModeDetails();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    GetPaymentModeDetails() {
        this.Visible = false;
        this.spinnerServ.show();
        this.MasterSer.GetPaymentModeMaster().subscribe((data: any) => {
            if (data.success) {
                this.PaymentModeList = data.getPaymentModeMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    OnChangePaymentMode() {
        this.PaymentModeExist = this.PaymentModeList.some((i: any) => i.paymentMode.toLowerCase() == this.PaymentModeForm.controls['PaymentMode'].value.trim().toLowerCase() && i.paymentModeId != this.PaymentModeId);
    }
    AddNew(key, PaymentModeid) {
        $('#AddPaymentModeModal').on('shown.bs.modal', function () {
            $('#add').focus()
        })
        $('#InActivePaymentModeModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        this.PaymentModeExist = false;
        this.PaymentModeId = PaymentModeid;
        this.Remarks = '';
        this.PaymentModeForm.reset();
        this.Submitted = false;
        if (key == 'Edit') {
            this.MasterSer.GetPaymentModeDetailsById(this.PaymentModeId).subscribe((data: any) => {
                if (data.success) {
                    this.PaymentModeForm.patchValue({
                        PaymentMode: data.getPaymentModeMasterById[0].paymentMode,
                    })
                    this.PaymentModeId = data.getPaymentModeMasterById[0].paymentModeId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.PaymentModeForm.invalid || this.PaymentModeExist) {
            return;
        }
        let inputObj = {
            paymentModeId: this.PaymentModeId,
            paymentMode: this.PaymentModeForm.controls['PaymentMode'].value.trim(),
            createdBy: this.LoginUserId
        }
        console.log(inputObj)
        this.spinnerServ.show();
        this.MasterSer.InsertPaymentModeMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Payment Mode Details" + (this.PaymentModeId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.spinnerServ.hide();
            this.GetPaymentModeDetails();
            document.getElementById('closeAddPaymentModeModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeletePaymentModeMaster(this.PaymentModeId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'PaymentMode Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetPaymentModeDetails();
            document.getElementById('closeInActivePaymentModeModal').click();
        })
    }
}