import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { MasterRoutingModule } from './master-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { UnitMasterComponent } from './unit-master/unit-master.component';
import { GstMasterComponent } from './gst-master/gst-master.component';
import { EmployeeMasterComponent } from './employee-master/employee-master.component';
import { ServiceMasterComponent } from './service-master/service-master.component';
import { MakeMasterComponent } from './make-master/make-master.component';
import { ModelMasterComponent } from './model-master/model-master.component';
import { PaymentModeMasterComponent } from './payment-mode-master/payment-mode-master.component';
import { ViewCompanyMasterComponent } from './view-company-master/view-company-master.component';
import { ViewUserMasterComponent } from './view-user-master/view-user-master.component';
import { CreateUserMasterComponent } from './create-user-master/create-user-master.component';
import { CreateCompanyMasterComponent } from './create-company-master/create-company-master.component';
import { ViewProductMasterComponent } from './view-product-master/view-product-master.component';
import { CreateProductMasterComponent } from './create-product-master/create-product-master.component';


@NgModule({
  declarations: [
  UnitMasterComponent,
  GstMasterComponent,
  EmployeeMasterComponent,
  ServiceMasterComponent,
  MakeMasterComponent,
  ModelMasterComponent,
  PaymentModeMasterComponent,
  ViewCompanyMasterComponent,
  ViewUserMasterComponent,
  CreateUserMasterComponent,
  CreateCompanyMasterComponent,
  ViewProductMasterComponent,
  CreateProductMasterComponent,

],
  imports: [
    CommonModule,
    MasterRoutingModule,
    ReactiveFormsModule,
    FormsModule, DataTablesModule, NgSelectModule,
    JwBootstrapSwitchNg2Module
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA]
})
export class MasterModule { }
