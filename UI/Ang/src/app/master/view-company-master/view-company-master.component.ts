import { Component, OnInit } from '@angular/core';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-view-company-master',
  templateUrl: './view-company-master.component.html',
  styleUrls: ['./view-company-master.component.css']
})
export class ViewCompanyMasterComponent implements OnInit {
  CompanyList: any = [];
  Visible: boolean = false;
  CompanyId: any;
  Submitted: boolean = false;
  Remarks: any; //NgModel
  menuList: any = [];
  LoginUserId: any;

  constructor(private MasterSer: MasterService, private SpinnerSer: NgxSpinnerService, private SharedSer: SharedService) { }

  ngOnInit(): void {
    this.GetCompanyMaster();
    if (localStorage.getItem('LoginUserDtls') != null) {
      var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
      this.menuList = LoginUserDtls[0].allocateMenu.split(',');
      if (localStorage.getItem('UserType') == "User") {
        this.LoginUserId = localStorage.getItem('userId');
      }
    }
  }

  GetCompanyMaster() {
    this.Visible = false;
    this.CompanyList = [];
    this.SpinnerSer.show();
    this.MasterSer.GetCompanyMaster().subscribe((data: any) => {
      if (data.success) {
        this.CompanyList = data.getCompanyMaster;
        this.Visible = true;
      }
      else {
        this.CompanyList = [];
        this.Visible = true;
      }
      this.SpinnerSer.hide();
    })
  }

  EditDelete(CompanyId) {
    $('#InActiveUnitModal').on('shown.bs.modal', function () {
      $('#delete').focus()
  })
    this.Remarks = "";
    this.Submitted = false;
    this.CompanyId = CompanyId;
  }

  Delete() {
    this.Submitted = true;
    if (this.Remarks == '' || this.Remarks.trim() == '') {
      return
    }
    this.SpinnerSer.show();
    this.MasterSer.DeleteCompanyMaster(this.CompanyId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
      if (data.success) {
        this.SharedSer.showNotification('top', 'center', 'Company Details Deleted Successfully', 'danger');
      }
      else {
        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
      }
      this.SpinnerSer.hide();
      this.GetCompanyMaster();
      document.getElementById('closeInActiveUnitModal').click();
    })
  }

}
