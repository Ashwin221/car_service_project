import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-employee-master',
    templateUrl: './employee-master.component.html',
    styleUrls: ['./employee-master.component.css']
})
export class EmployeeMasterComponent implements OnInit {
    EmployeeId: any = 0;
    EmployeeList: any = [];
    EmployeeForm: FormGroup;
    Remarks: any = ';'
    Visible: boolean = false;
    Submitted: boolean = false;
    LoginUserId: any = 0;
    menuList: any = [];
    EmployeeCodeExists: boolean = false;
    EmployeeNameExists: boolean = false;

    constructor(private fb: FormBuilder, private MasterSer: MasterService,
        private spinnerServ: NgxSpinnerService, private SharedSer: SharedService) {
        this.EmployeeForm = this.fb.group({
            employeeName: ['', Validators.required],
            employeeCode: ['', Validators.required],
            address: ['', Validators.required],
            mobileNo: ['', Validators.required],
        })
    }

    ngOnInit(): void {
        this.GetEmployeeDetails();
        var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
        this.menuList = LoginUserDtls[0].allocateMenu.split(',');
        if (localStorage.getItem('UserType') == "User") {
            this.LoginUserId = localStorage.getItem('userId');
        }
    }

    GetEmployeeDetails() {
        this.Visible = false;
        this.spinnerServ.show();
        this.MasterSer.GetEmployeeMaster().subscribe((data: any) => {
            if (data.success) {
                this.EmployeeList = data.getEmployeeMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    OnChangeEmployee(Key: any) {
        if (Key == "EmployeeCode") {
            this.EmployeeCodeExists = this.EmployeeList.some((i: any) => i.employeeCode.toLowerCase() == this.EmployeeForm.controls['employeeCode'].value.trim().toLowerCase() && i.employeeId != this.EmployeeId);
        }
        if (Key == "EmployeeName") {
            this.EmployeeNameExists = this.EmployeeList.some((i: any) => i.employeeName.toLowerCase() == this.EmployeeForm.controls['employeeName'].value.trim().toLowerCase() && i.employeeId != this.EmployeeId);
        }
    }

    AddNew(key, Employeeid) {
        $('#AddEmployeeModal').on('shown.bs.modal', function () {
            $('#add').focus()
        })
        $('#InActiveEmployeeModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        this.EmployeeNameExists = false;
        this.EmployeeCodeExists = false;
        this.EmployeeId = Employeeid;
        this.Remarks = '';
        this.EmployeeForm.reset();
        this.Submitted = false;
        if (key == 'Edit') {
            this.MasterSer.GetEmployeeMasterById(this.EmployeeId).subscribe((data: any) => {
                if (data.success) {
                    this.EmployeeForm.patchValue({
                        employeeName: data.getEmployeeMasterById[0].employeeName,
                        employeeCode: data.getEmployeeMasterById[0].employeeCode,
                        address: data.getEmployeeMasterById[0].address,
                        mobileNo: data.getEmployeeMasterById[0].mobileNo,
                    })
                    this.EmployeeId = data.getEmployeeMasterById[0].employeeId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.EmployeeForm.invalid || this.EmployeeNameExists || this.EmployeeCodeExists) {
            return;
        }
        let inputObj = {
            employeeId: this.EmployeeId,
            employeeCode: this.EmployeeForm.controls['employeeCode'].value.trim(),
            employeeName: this.EmployeeForm.controls['employeeName'].value.trim(),
            address: this.EmployeeForm.controls['address'].value.trim(),
            mobileNo: this.EmployeeForm.controls['mobileNo'].value.toString(),
            createdBy: this.LoginUserId
        }    
        console.log(inputObj)    
        this.spinnerServ.show();
        this.MasterSer.InsertEmployeeMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Employee Details" + (this.EmployeeId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.spinnerServ.hide();
            this.GetEmployeeDetails();
            document.getElementById('closeAddEmployeeModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeleteEmployeeMaster(this.EmployeeId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Employee Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetEmployeeDetails();
            document.getElementById('closeInActiveEmployeeModal').click();
        })
    }

}

