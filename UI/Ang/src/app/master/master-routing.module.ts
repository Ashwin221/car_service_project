import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnitMasterComponent } from './unit-master/unit-master.component';
import { GstMasterComponent } from './gst-master/gst-master.component';
import { EmployeeMasterComponent } from './employee-master/employee-master.component';
import { ServiceMasterComponent } from './service-master/service-master.component';
import { CreateCompanyMasterComponent } from './create-company-master/create-company-master.component';
import { ViewCompanyMasterComponent } from './view-company-master/view-company-master.component';
import { CreateUserMasterComponent } from './create-user-master/create-user-master.component';
import { ViewUserMasterComponent } from './view-user-master/view-user-master.component';
import { MakeMasterComponent } from './make-master/make-master.component';
import { ModelMasterComponent } from './model-master/model-master.component';
import { PaymentModeMasterComponent } from './payment-mode-master/payment-mode-master.component';
import { ViewProductMasterComponent } from './view-product-master/view-product-master.component';
import { CreateProductMasterComponent } from './create-product-master/create-product-master.component';


const routes: Routes = [
  {path:"unit-master",component:UnitMasterComponent},
  {path:"gst-master",component:GstMasterComponent},
  {path:"create-product-master",component:CreateProductMasterComponent},
  {path:"view-product-master",component:ViewProductMasterComponent},
  {path:"employee-master",component:EmployeeMasterComponent},
  {path:"service-master",component:ServiceMasterComponent},
  {path:"create-company-master",component:CreateCompanyMasterComponent},
  {path:"view-company-master",component:ViewCompanyMasterComponent},
  {path:"create-user-master",component:CreateUserMasterComponent},
  {path:"view-user-master",component:ViewUserMasterComponent},
  {path:"make-master",component:MakeMasterComponent},
  {path:"model-master",component:ModelMasterComponent},
  {path:"payment-mode-master",component:PaymentModeMasterComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }