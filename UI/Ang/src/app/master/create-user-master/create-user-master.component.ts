import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-create-user-master',
    templateUrl: './create-user-master.component.html',
    styleUrls: ['./create-user-master.component.css']
})
export class CreateUserMasterComponent implements OnInit {
    UnitList: any = [];
    UserForm: FormGroup;
    ItemId: any = 0;
    MenuList: any = [];
    SelectedMenus: any = [];
    Submitted: boolean = false;
    UserId: any = 0;
    LoginUserId: any = 0;
    EmployeeList: any = [];
    EmployeeNameExists: boolean = false;
    UserNameExists: boolean = false;
    UserList: any = [];
    menuList: any = [];

    constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService, private SpinnerSer: NgxSpinnerService,
        private Router: Router, private activateRoute: ActivatedRoute) {
        this.UserForm = this.fb.group({
            employee: [null, Validators.required],
            userName: ['', Validators.required],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            userType: ['', Validators.required],

        })
    }

    async ngOnInit() {
        await this.GetEmployeeDetails();
        await this.GetMenuList();
        if (this.activateRoute.snapshot.queryParamMap.get('UserId') != null) {
            this.UserId = this.activateRoute.snapshot.queryParamMap.get('UserId');
            this.GetUserMasterById();
        }
        this.GetUserMaster();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    GetUserMaster() {
        this.MasterSer.GetUserMaster().subscribe((data: any) => {
            if (data.success) {
                this.UserList = data.getUserMaster;
            }
        })
    }

    OnChange(Key: any) {
        if (Key == "EmployeeName") {
            this.EmployeeNameExists = this.UserList.some((i: any) => i.employeeName == this.UserForm.controls['employee'].value.employeeName && i.userId != this.UserId);
        }
        if (Key == "UserName") {
            this.UserNameExists = this.UserList.some((i: any) => i.userName.toLowerCase() == this.UserForm.controls['userName'].value.trim().toLowerCase() && i.userId != this.UserId);
        }
    }

    GetUserMasterById() {
        this.MasterSer.GetUserMasterById(this.UserId).subscribe((data: any) => {
            if (data.success) {
                this.UserForm.patchValue({
                    employee: this.EmployeeList.find(f => f.employeeId == data.getUserMasterById[0].employeeId),
                    userName: data.getUserMasterById[0].userName,
                    password: data.getUserMasterById[0].password,
                    confirmPassword: data.getUserMasterById[0].confirmPassword,
                    userType: data.getUserMasterById[0].userType
                })
                this.SelectedMenus = (data.getUserMasterById[0].allocateMenu).split(',');
                this.SelectedMenus = this.SelectedMenus.filter(k => k !== '');

            }
        })
    }

    async GetEmployeeDetails() {
        await this.MasterSer.GetEmployeeMaster().toPromise().then((data: any) => {
            if (data.success) {
                this.EmployeeList = data.getEmployeeMaster;
            }
        })
    }
    async GetMenuList() {
        await this.MasterSer.GetMenuMasterDtls().toPromise().then((data: any) => {
            if (data.success) {
                let FilterMenus = data.getMenuMaster.filter(k => k.subMenuId != "");
                this.MenuList = FilterMenus.reduce((r, { header }) => {
                    if (!r.some(o => o.header == header)) {
                        r.push({ header, groupItem: FilterMenus.filter(v => v.header == header) });
                    }
                    return r;
                }, []);
            }
        })
    }

    SelectAll(event, i) {
        for (var k = 0; k < this.MenuList[i].groupItem.length; k++) {
            this.OnMenuChecked(event, this.MenuList[i].groupItem[k].subMenuIds.split(',')[0])
            this.OnMenuChecked(event, this.MenuList[i].groupItem[k].subMenuIds.split(',')[1])
            this.OnMenuChecked(event, this.MenuList[i].groupItem[k].subMenuIds.split(',')[2])
            this.OnMenuChecked(event, this.MenuList[i].groupItem[k].subMenuIds.split(',')[3])
        }
    }
    OnMenuChecked(event, value) {
        if (event.target.checked) {
            if (this.SelectedMenus.indexOf(value) == -1) {
                this.SelectedMenus.push(value);
            }
        }
        else {
            if (this.SelectedMenus.indexOf(value) > -1) {
                this.SelectedMenus.splice(this.SelectedMenus.indexOf(value), 1);
            }
        }
    }

    Save() {
        this.Submitted = true;
        if (this.UserForm.invalid || this.UserForm.controls['password'].value.trim() != this.UserForm.controls['confirmPassword'].value.trim()
            || this.UserNameExists || this.EmployeeNameExists) {
            return;
        }
        if (this.SelectedMenus.length == 0) {
            this.SharedSer.showNotification('top', 'center', 'Please Select Atleast One Menu', 'info')
            return;
        }
        let inputObj = {
            userId: Number(this.UserId),
            employeeId: this.UserForm.controls['employee'].value['employeeId'],
            employeeName: this.UserForm.controls['employee'].value['employeeName'],
            userType: this.UserForm.controls['userType'].value,
            userName: this.UserForm.controls['userName'].value.trim(),
            password: this.UserForm.controls['password'].value.trim(),
            confirmPassword: this.UserForm.controls['confirmPassword'].value.trim(),
            allocateMenu: this.SelectedMenus.join(','),
            createdBy: Number(localStorage.getItem("userId"))
        }
        console.log(inputObj)
        this.SpinnerSer.show();
        this.MasterSer.InsertUserMaster(inputObj).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', "User Details" + (this.UserId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.Router.navigate(['/master/view-user-master'])
            this.SpinnerSer.hide();
        })

    }
}
