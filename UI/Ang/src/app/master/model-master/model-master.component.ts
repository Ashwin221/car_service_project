import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-model-master',
    templateUrl: './model-master.component.html',
    styleUrls: ['./model-master.component.css']
})
export class ModelMasterComponent implements OnInit {
    ModelId: any = 0;
    ModelList: any = [];
    ModelForm: FormGroup;
    Submitted: boolean = false;
    LoginUserId: any = 0;
    Visible: boolean = false;
    Remarks: any = '';
    MakeList: any = [];
    menuList: any = [];
    ModelExists: boolean = false;

    constructor(private fb: FormBuilder, private MasterSer: MasterService, private spinnerServ: NgxSpinnerService,
        private SharedSer: SharedService) {
        this.ModelForm = this.fb.group({
            make: [null, Validators.required],
            model: ['', Validators.required],
        })
    }

    ngOnInit(): void {
        this.GetMakeDetails();
        this.GetModelDetails();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    GetMakeDetails() {
        this.MasterSer.GetMakeMaster().subscribe((data: any) => {
            if (data.success) {
                this.MakeList = data.getMakeMaster;
            }
        })
    }

    GetModelDetails() {
        this.Visible = false;
        this.spinnerServ.show();
        this.MasterSer.GetModelMaster().subscribe((data: any) => {
            if (data.success) {
                this.ModelList = data.getModelMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    OnChangeModel() {
        this.ModelExists = this.ModelList.some((i: any) => i.make == this.ModelForm.controls['make'].value.make && i.model.toLowerCase() == this.ModelForm.controls['model'].value.trim().toLowerCase() && i.modelId != this.ModelId);
    }

    AddNew(key, Modelid) {
        $('#AddModelModal').on('shown.bs.modal', function () {
            $('#add').focus()
        })
        $('#InActiveModelModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        this.ModelExists = false;
        this.ModelId = Modelid;
        this.Remarks = '';
        this.ModelForm.reset();
        this.Submitted = false;
        if (key == 'Edit') {
            this.MasterSer.GetModelDetailsById(this.ModelId).subscribe((data: any) => {
                if (data.success) {
                    this.ModelForm.patchValue({
                        make: this.MakeList.find(f => f.makeId == data.getModelMasterById[0].makeId),
                        model: data.getModelMasterById[0].model,
                    })
                    this.ModelId = data.getModelMasterById[0].modelId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.ModelForm.invalid || this.ModelExists) {
            return;
        }

        let inputObj = {
            modelId: this.ModelId,
            makeId: this.ModelForm.controls['make'].value['makeId'],
            make: this.ModelForm.controls['make'].value['make'],
            model: this.ModelForm.controls['model'].value.trim(),
            createdBy: this.LoginUserId
        }
        console.log(inputObj)

        this.spinnerServ.show();
        this.MasterSer.InsertModelMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Model Details" + (this.ModelId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.spinnerServ.hide();
            this.GetModelDetails();
            document.getElementById('closeAddModelModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeleteModelMaster(this.ModelId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Model Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetModelDetails();
            document.getElementById('closeInActiveModelModal').click();
        })
    }


}