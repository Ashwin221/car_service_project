import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-gst-master',
    templateUrl: './gst-master.component.html',
    styleUrls: ['./gst-master.component.css']
})
export class GstMasterComponent implements OnInit {
    GstId: any = 0;
    GSTList: any = [];
    GSTForm: FormGroup;
    Submitted: boolean = false;
    LoginUserId: any = 0;
    Visible: boolean = false;
    Remarks: any = '';
    GSTNameExists: boolean = false;
    GSTValueExists: boolean = false;
    menuList: any = [];
    constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService, private spinnerServ: NgxSpinnerService) {
        this.GSTForm = this.fb.group({
            gstValue: ['', Validators.required],
            gstName: ['', Validators.required]
        })
    }

    ngOnInit(): void {
        this.GetGstDetails();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    GetGstDetails() {
        this.Visible = false;
        this.spinnerServ.show();
        this.MasterSer.GetGstMaster().subscribe((data: any) => {
            if (data.success) {
                this.GSTList = data.getGSTMaster;
            }
            this.Visible = true;
            this.spinnerServ.hide();
        })
    }

    OnChangeGST(Key: any) {
        if (Key == "GSTName") {
            this.GSTNameExists = this.GSTList.some((i: any) => i.gstName.toLowerCase() == this.GSTForm.controls['gstName'].value.trim().toLowerCase() && i.gstId != this.GstId);
        }
        if (Key == "GSTValue") {
            this.GSTValueExists = this.GSTList.some((i: any) => i.gstValue == this.GSTForm.controls['gstValue'].value && i.gstId != this.GstId);
        }
    }

    AddNew(key, Gstid) {
        $('#AddGSTModal').on('shown.bs.modal', function () {
            $('#add').focus()
        });
        $('#InActiveGstModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        });
        this.GstId = Gstid;
        this.Remarks = '';
        this.GSTNameExists = false;
        this.GSTValueExists = false;
        this.GSTForm.reset();
        this.Submitted = false;

        if (key == 'Edit') {
            this.MasterSer.GetGstDetailsById(this.GstId).subscribe((data: any) => {
                if (data.success) {
                    this.GSTForm.patchValue({
                        gstName: data.getGSTMasterById[0].gstName,
                        gstValue: data.getGSTMasterById[0].gstValue,
                    })
                    this.GstId = data.getGSTMasterById[0].gstId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.GSTForm.invalid || this.GSTValueExists || this.GSTNameExists) {
            return;
        }
        let inputObj = {
            gstId: this.GstId,
            gstValue: this.GSTForm.controls['gstValue'].value.toString(),
            gstName: this.GSTForm.controls['gstName'].value.trim(),
            createdBy: this.LoginUserId
        }
        console.log(inputObj)
        this.spinnerServ.show();
        this.MasterSer.InsertGstMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Gst Details" + (this.GstId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.spinnerServ.hide();
            this.GetGstDetails();
            document.getElementById('closeAddGSTModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.spinnerServ.show();
        this.MasterSer.DeleteGstMaster(this.GstId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Gst Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.spinnerServ.hide();
            this.GetGstDetails();
            document.getElementById('closeInActiveGstModal').click();
        })
    }


}
