import { Component, OnInit } from '@angular/core';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any
@Component({
  selector: 'app-view-user-master',
  templateUrl: './view-user-master.component.html',
  styleUrls: ['./view-user-master.component.css']
})
export class ViewUserMasterComponent implements OnInit {
  UserList: any = [];
  Visible: boolean = false;
  UserId: any = 0;
  Submitted: boolean = false;
  Remarks: any = '';
  LoginUserId: any = 0;
  menuList: any = [];

  constructor(private MasterSer: MasterService, private SpinnerSer: NgxSpinnerService,
    private SharedSer: SharedService) { }

  ngOnInit(): void {
    this.GetUserMaster();
    if (localStorage.getItem('LoginUserDtls') != null) {
      var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
      this.menuList = LoginUserDtls[0].allocateMenu.split(',');
      if (localStorage.getItem('UserType') == "User") {
        this.LoginUserId = localStorage.getItem('userId');
      }
    }
  }
  GetUserMaster() {
    this.Visible = false;
    this.SpinnerSer.show();
    this.MasterSer.GetUserMaster().subscribe((data: any) => {
      if (data.success) {
        this.UserList = data.getUserMaster;
      }
      this.Visible = true;
      this.SpinnerSer.hide();
    })
  }

  Delete(UserId, Key) {
    $('#InActiveUserModal').on('shown.bs.modal', function () {
      $('#delete').focus()
    })
    if (Key == 'Assign') {
      this.UserId = UserId;
      this.Submitted = false;
      this.Remarks = "";
      $("#InActiveUserModal").modal('show');
      return;
    }

    this.Submitted = true;
    if (this.Remarks == '' || this.Remarks.trim() == '') {
      return
    }
    this.SpinnerSer.show();
    this.MasterSer.DeleteUserMaster(this.UserId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
      if (data.success) {
        this.SharedSer.showNotification('top', 'center', 'User Details Deleted Successfully', 'danger');
      }
      else {
        this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
      }
      this.SpinnerSer.hide();
      this.GetUserMaster();
      document.getElementById('closeInActiveUserModal').click();
    })
  }
}
