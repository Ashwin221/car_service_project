import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MasterService } from 'app/Services/master.service';
import { SharedService } from 'app/Services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-service-master',
    templateUrl: './service-master.component.html',
    styleUrls: ['./service-master.component.css']
})
export class ServiceMasterComponent implements OnInit {
    ServiceId: any = 0;
    ServiceList: any = [];
    ServiceForm: FormGroup;
    Submitted: boolean = false;
    GSTList: any = [];
    LoginUserId: any = 0;
    Remarks: any = '';
    Visible: boolean = false;
    menuList: any = [];
    ServiceTypeExists: boolean = false;

    constructor(private fb: FormBuilder, private MasterSer: MasterService, private SharedSer: SharedService, private SpinnerSer: NgxSpinnerService) {
        this.ServiceForm = this.fb.group({
            category: ['', Validators.required],
            serviceType: ['', Validators.required],
            price: ['', Validators.required],
            gst: [null, Validators.required]
        })
    }

    ngOnInit(): void {
        this.GetServiceDetails();
        this.GetGstDetails();
        if (localStorage.getItem('LoginUserDtls') != null) {
            var LoginUserDtls = JSON.parse(localStorage.getItem('LoginUserDtls'));
            this.menuList = LoginUserDtls[0].allocateMenu.split(',');
            if (localStorage.getItem('UserType') == "User") {
                this.LoginUserId = localStorage.getItem('userId');
            }
        }
    }

    GetGstDetails() {
        this.MasterSer.GetGstMaster().subscribe((data: any) => {
            if (data.success) {
                this.GSTList = data.getGSTMaster;
            }
        })
    }

    GetServiceDetails() {
        this.Visible = false;
        this.SpinnerSer.show();
        this.MasterSer.GetServiceMaster().subscribe((data: any) => {
            if (data.success) {
                this.ServiceList = data.getServiceMaster;
            }
            this.Visible = true;
            this.SpinnerSer.hide();
        })
    }


    OnChangeServiceType() {
        this.ServiceTypeExists = this.ServiceList.some((i: any) => i.serviceType.toLowerCase() == this.ServiceForm.controls['serviceType'].value.trim().toLowerCase() && i.serviceId != this.ServiceId);
    }

    AddNew(key, Serviceid) {
        $('#AddServiceModal').on('shown.bs.modal', function () {
            $('#add').focus()
        })
        $('#InActiveServiceModal').on('shown.bs.modal', function () {
            $('#delete').focus()
        })
        this.ServiceTypeExists = false;
        this.ServiceId = Serviceid;
        this.Remarks = '';
        this.ServiceForm.reset();
        this.Submitted = false;
        this.ServiceForm.controls['category'].setValue("");
        if (key == 'Edit') {
            this.MasterSer.GetServiceMasterById(this.ServiceId).subscribe((data: any) => {
                if (data.success) {
                    this.ServiceForm.patchValue({
                        category: data.getServiceMasterById[0].category,
                        serviceType: data.getServiceMasterById[0].serviceType,
                        price: data.getServiceMasterById[0].price,
                        gst: this.GSTList.find(f => f.gstId == data.getServiceMasterById[0].gstId),
                    })
                    this.ServiceId = data.getServiceMasterById[0].serviceId
                }
            })
        }
    }

    Save() {
        this.Submitted = true;
        if (this.ServiceForm.invalid || this.ServiceTypeExists) {
            return;
        }
        let inputObj = {

            serviceId: this.ServiceId,
            category: this.ServiceForm.controls['category'].value,
            serviceType: this.ServiceForm.controls['serviceType'].value.trim(),
            price: this.ServiceForm.controls['price'].value.toString(),
            gstId: this.ServiceForm.controls['gst'].value['gstId'],
            gstName: this.ServiceForm.controls['gst'].value['gstName'],
            gstValue: this.ServiceForm.controls['gst'].value['gstValue'],
            createdBy: this.LoginUserId
        }
        console.log(inputObj)
        this.SpinnerSer.show();
        this.MasterSer.InsertServiceMaster(inputObj).subscribe((data: any) => {
            if (data.success) {

                this.SharedSer.showNotification('top', 'center', "Service Details" + (this.ServiceId == 0 ? ' Save' : ' Update') + "d Successfully.", "success");
            } else {

                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger')
            }
            this.SpinnerSer.hide();
            this.GetServiceDetails();
            document.getElementById('closeAddServiceModal').click();
        })
    }

    Delete() {
        this.Submitted = true;
        if (this.Remarks == '' || this.Remarks.trim() == '') {
            return
        }
        this.SpinnerSer.show();
        this.MasterSer.DeleteServiceMaster(this.ServiceId, this.LoginUserId, this.Remarks).subscribe((data: any) => {
            if (data.success) {
                this.SharedSer.showNotification('top', 'center', 'Service Details Deleted Successfully', 'danger');
            }
            else {
                this.SharedSer.showNotification('top', 'center', 'Oops.. Please Try after Sometime', 'danger');
            }
            this.SpinnerSer.hide();
            this.GetServiceDetails();
            document.getElementById('closeInActiveServiceModal').click();
        })
    }

}