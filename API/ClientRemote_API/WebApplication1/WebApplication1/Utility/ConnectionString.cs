﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Utility
{
    public class ConnectionString
    {
        //Local
        
        //public static string cName = "Data Source=DESKTOP-H2GFMHO\\SQLEXPRESS;Initial Catalog=CarWash;User ID=sa;Password=sa123";
        //public static string cName = "Data Source=DESKTOP-O0QMUHV;Initial Catalog=CarWash_07Jul;User ID=sa;Password=sa123";
        //public static string cName = "Data Source=DESKTOP-O0QMUHV;Initial Catalog=CarWash_29Jun;User ID=sa;Password=sa123";


        // Test
        //private static string cName = "Data Source=S184-168-121-23;Initial Catalog=CarWash_Test;User ID=sa;Password=Elevado@1108";

        //Live

        //private static string cName = "Data Source=S184-168-121-23;Initial Catalog=CarWash;User ID=sa;Password=Elevado@1108";
        private static string cName = "Data Source=DESKTOP-26T4A28\\SQLEXPRESS;Initial Catalog=CarWash;User ID=sa;Password=carwash123";

        public static string CName { get => cName; }
    }
}
