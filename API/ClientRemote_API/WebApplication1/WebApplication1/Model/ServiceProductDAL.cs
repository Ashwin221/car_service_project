﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.AppCode;

namespace WebApplication1.Model
{
    public class ServiceProductDAL
    {

        public string InsertServiceProduct(int Service_ProductId, string Service_ProductNo, string Type, string Date, int CustomerId, string MobileNo, string CustomerName, string VehicleRegNo,
        int MakeId, string Make, int ModelId, string Model, string DeliveryDateTime, string ServiceDoneById, string ServiceDoneBy, string SalesDoneById,
        string SalesDoneBy, string OverAllTotal, string OverAllTax, string DiscountFromTotal, string FinalInvAmt, int CreatedBy, DataTable ServiceProductSub
        )
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertServiceProduct");
                objSql.AddParameter(objCmd, "@Service_ProductId", SqlDbType.Int, ParameterDirection.Input, 20, Service_ProductId);
                objSql.AddParameter(objCmd, "@Service_ProductNo", SqlDbType.VarChar, ParameterDirection.Input, 30, Service_ProductNo);
                objSql.AddParameter(objCmd, "@Type", SqlDbType.VarChar, ParameterDirection.Input, 30, Type);
                objSql.AddParameter(objCmd, "@Date", SqlDbType.VarChar, ParameterDirection.Input, 20, Date);
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, CustomerId);
                objSql.AddParameter(objCmd, "@MobileNo", SqlDbType.VarChar, ParameterDirection.Input, 20, MobileNo);
                objSql.AddParameter(objCmd, "@CustomerName", SqlDbType.VarChar, ParameterDirection.Input, 50, CustomerName);
                objSql.AddParameter(objCmd, "@VehicleRegNo", SqlDbType.VarChar, ParameterDirection.Input, 20, VehicleRegNo);
                objSql.AddParameter(objCmd, "@MakeId", SqlDbType.Int, ParameterDirection.Input, 20, MakeId);
                objSql.AddParameter(objCmd, "@Make", SqlDbType.VarChar, ParameterDirection.Input, 20, Make);
                objSql.AddParameter(objCmd, "@ModelId", SqlDbType.Int, ParameterDirection.Input, 20, ModelId);
                objSql.AddParameter(objCmd, "@Model", SqlDbType.VarChar, ParameterDirection.Input, 20, Model);
                objSql.AddParameter(objCmd, "@DeliveryDateTime", SqlDbType.VarChar, ParameterDirection.Input, 30, DeliveryDateTime);
                objSql.AddParameter(objCmd, "@ServiceDoneById", SqlDbType.VarChar, ParameterDirection.Input, 50, ServiceDoneById);
                objSql.AddParameter(objCmd, "@ServiceDoneBy", SqlDbType.VarChar, ParameterDirection.Input, 30, ServiceDoneBy);
                objSql.AddParameter(objCmd, "@SalesDoneById", SqlDbType.VarChar, ParameterDirection.Input, 50, SalesDoneById);
                objSql.AddParameter(objCmd, "@SalesDoneBy", SqlDbType.VarChar, ParameterDirection.Input, 30, SalesDoneBy);
                objSql.AddParameter(objCmd, "@OverAllTotal", SqlDbType.VarChar, ParameterDirection.Input, 30, OverAllTotal);
                objSql.AddParameter(objCmd, "@OverAllTax", SqlDbType.VarChar, ParameterDirection.Input, 30, OverAllTax);
                objSql.AddParameter(objCmd, "@DiscountFromTotal", SqlDbType.VarChar, ParameterDirection.Input, 30, DiscountFromTotal);
                objSql.AddParameter(objCmd, "@FinalInvAmt", SqlDbType.VarChar, ParameterDirection.Input, 30, FinalInvAmt);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, CreatedBy);

                objSql.AddParameter(objCmd, "@ServiceProductSub", SqlDbType.Structured, ParameterDirection.Input, -1, ServiceProductSub);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        //FOR TEST
        public DataTable InsertServiceProductdt(DataTable ServiceProductSub, int CustomerId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertServiceProductdt");
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, CustomerId);
                objSql.AddParameter(objCmd, "@ServiceProductSub", SqlDbType.Structured, ParameterDirection.Input, -1, ServiceProductSub);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public DataTable GetVehicleRegNoDtls(int CustomerId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetVehicleRegNoDtls");
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, CustomerId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public DataTable GetProductServiceDtls()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetProductServiceDtls");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public DataSet GetProductServiceDtlsById(int ServiceProductId)
        {
            DataSet dt = new DataSet();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetProductServiceDtlsById");
                objSql.AddParameter(objCmd, "@Service_ProductId", SqlDbType.Int, ParameterDirection.Input, 20, ServiceProductId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteProductServiceMaster(int ServiceProductId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteProductServiceMaster");
                objSql.AddParameter(objCmd, "@Service_ProductId", SqlDbType.Int, ParameterDirection.Input, 20, ServiceProductId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public string EligibleForFreeWash(string MobileNo, string Category)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter(); ;
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ServiceProductSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "EligibleForFreeWash");
                objSql.AddParameter(objCmd, "@MobileNo", SqlDbType.VarChar, ParameterDirection.Input, 20, MobileNo);
                objSql.AddParameter(objCmd, "@Category", SqlDbType.VarChar, ParameterDirection.Input, 30, Category);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
    }
}
