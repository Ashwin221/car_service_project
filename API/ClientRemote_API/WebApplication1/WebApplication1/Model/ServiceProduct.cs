﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class ServiceProduct
    {
        public int service_ProductId { get; set; }
        public string service_ProductNo { get; set; }
        public string type { get; set; }
        public string date { get; set; }
        public int customerId { get; set; }
        public string mobileNo { get; set; }
        public string customerName { get; set; }
        public string vehicleRegNo { get; set; }
        public int makeId { get; set; }
        public string make { get; set; }
        public int modelId { get; set; }
        public string model { get; set; }
        public string deliveryDateTime { get; set; }
        public string serviceDoneById { get; set; }
        public string serviceDoneBy { get; set; }
        public string salesDoneById { get; set; }
        public string salesDoneBy { get; set; }
        public string overallTotal { get; set; }
        public string overAllTax { get; set; }
        public string discFromTotal { get; set; }
        public string finalAmount { get; set; }
        public string status { get; set; }
        public int createdBy { get; set; }
        public List<ServiceProductSub> serviceProductSub { get; set; }

    }

    public class ServiceProductSub
    {
        public string category { get; set; }
        public string type { get; set; }
        public int typeId { get; set; }
        public string typeName { get; set; }
        public string price { get; set; }
        public int qty { get; set; }
        public string amount { get; set; }
        public string discount { get; set; }
        public bool isAvailGst { get; set; }
        public int gstId { get; set; }
        public string gstName { get; set; }
        public string gstValue { get; set; }
        public string taxableAmt { get; set; }
        public string total { get; set; }
        public bool freeCarWash { get; set; }
        public bool freeBikeWash { get; set; }

    }
}
