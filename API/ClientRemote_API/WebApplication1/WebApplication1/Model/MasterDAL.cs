﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.AppCode;

namespace WebApplication1.Model
{
    public class MasterDAL
    {
        #region UnitMaster
        public string InsertUnitMaster(int unitId, string unitCode, string unitName, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertUnitMaster");
                objSql.AddParameter(objCmd, "@UnitId", SqlDbType.Int, ParameterDirection.Input, 20, unitId);
                objSql.AddParameter(objCmd, "@UnitCode", SqlDbType.VarChar, ParameterDirection.Input, 50, unitCode);
                objSql.AddParameter(objCmd, "@UnitName", SqlDbType.VarChar, ParameterDirection.Input, 50, unitName);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetUnitMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetUnitMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetUnitMasterById(int unitId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetUnitMasterById");
                objSql.AddParameter(objCmd, "@UnitId", SqlDbType.Int, ParameterDirection.Input, 20, unitId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteUnitMaster(int unitId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteUnitMaster");
                objSql.AddParameter(objCmd, "@UnitId", SqlDbType.Int, ParameterDirection.Input, 20, unitId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region GSTMaster
        public string InsertGSTMaster(int gstId, string gstName, string gstValue, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertGSTMaster");
                objSql.AddParameter(objCmd, "@GSTId", SqlDbType.Int, ParameterDirection.Input, 20, gstId);
                objSql.AddParameter(objCmd, "@GSTName", SqlDbType.VarChar, ParameterDirection.Input, 50, gstName);
                objSql.AddParameter(objCmd, "@GSTValue", SqlDbType.VarChar, ParameterDirection.Input, 50, gstValue);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetGSTMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetGSTMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetGSTMasterById(int gstId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetGSTMasterById");
                objSql.AddParameter(objCmd, "@GSTId", SqlDbType.Int, ParameterDirection.Input, 20, gstId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteGSTMaster(int gstId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteGSTMaster");
                objSql.AddParameter(objCmd, "@GSTId", SqlDbType.Int, ParameterDirection.Input, 20, gstId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region ProductMaster
        public string InsertProductMaster(int productId, string productCode, string productName, string description,
            int unitId, string unitName, string price, int gstId, string gstName, string gstValue, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertProductMaster");
                objSql.AddParameter(objCmd, "@ProductId", SqlDbType.Int, ParameterDirection.Input, 20, productId);
                objSql.AddParameter(objCmd, "@ProductCode", SqlDbType.VarChar, ParameterDirection.Input, 50, productCode);
                objSql.AddParameter(objCmd, "@ProductName", SqlDbType.VarChar, ParameterDirection.Input, 100, productName);
                objSql.AddParameter(objCmd, "@Description", SqlDbType.VarChar, ParameterDirection.Input, 300, description);
                objSql.AddParameter(objCmd, "@UnitId", SqlDbType.Int, ParameterDirection.Input, 20, unitId);
                objSql.AddParameter(objCmd, "@UnitName", SqlDbType.VarChar, ParameterDirection.Input, 50, unitName);
                objSql.AddParameter(objCmd, "@Price", SqlDbType.VarChar, ParameterDirection.Input, 20, price);
                objSql.AddParameter(objCmd, "@GSTId", SqlDbType.Int, ParameterDirection.Input, 20, gstId);
                objSql.AddParameter(objCmd, "@GSTName", SqlDbType.VarChar, ParameterDirection.Input, 50, gstName);
                objSql.AddParameter(objCmd, "@GSTValue", SqlDbType.VarChar, ParameterDirection.Input, 50, gstValue);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetProductMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetProductMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetProductMasterById(int productId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetProductMasterById");
                objSql.AddParameter(objCmd, "@ProductId", SqlDbType.Int, ParameterDirection.Input, 20, productId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteProductMaster(int productId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteProductMaster");
                objSql.AddParameter(objCmd, "@ProductId", SqlDbType.Int, ParameterDirection.Input, 20, productId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region EmployeeMaster
        public string InsertEmployeeMaster(int employeeId, string employeeCode, string employeeName, string address, string mobileNo, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertEmployeeMaster");
                objSql.AddParameter(objCmd, "@EmployeeId", SqlDbType.Int, ParameterDirection.Input, 20, employeeId);
                objSql.AddParameter(objCmd, "@EmployeeCode", SqlDbType.VarChar, ParameterDirection.Input, 50, employeeCode);
                objSql.AddParameter(objCmd, "@employeeName", SqlDbType.VarChar, ParameterDirection.Input, 100, employeeName);
                objSql.AddParameter(objCmd, "@Address", SqlDbType.VarChar, ParameterDirection.Input, 300, address);
                objSql.AddParameter(objCmd, "@MobileNo", SqlDbType.VarChar, ParameterDirection.Input, 20, mobileNo);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetEmployeeMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetEmployeeMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetEmployeeMasterById(int employeeId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetEmployeeMasterById");
                objSql.AddParameter(objCmd, "@EmployeeId", SqlDbType.Int, ParameterDirection.Input, 20, employeeId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteEmployeeMaster(int employeeId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteEmployeeMaster");
                objSql.AddParameter(objCmd, "@EmployeeId", SqlDbType.Int, ParameterDirection.Input, 20, employeeId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region ServiceMaster
        public string InsertServiceMaster(int serviceId, string category, string serviceType, string price, int gstId, string gstName, string gstValue, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertServiceMaster");
                objSql.AddParameter(objCmd, "@serviceId", SqlDbType.Int, ParameterDirection.Input, 20, serviceId);
                objSql.AddParameter(objCmd, "@Category", SqlDbType.VarChar, ParameterDirection.Input, 20, category);
                objSql.AddParameter(objCmd, "@ServiceType", SqlDbType.VarChar, ParameterDirection.Input, 50, serviceType);
                objSql.AddParameter(objCmd, "@Price", SqlDbType.VarChar, ParameterDirection.Input, 20, price);
                objSql.AddParameter(objCmd, "@GSTId", SqlDbType.Int, ParameterDirection.Input, 20, gstId);
                objSql.AddParameter(objCmd, "@GSTName", SqlDbType.VarChar, ParameterDirection.Input, 50, gstName);
                objSql.AddParameter(objCmd, "@GSTValue", SqlDbType.VarChar, ParameterDirection.Input, 20, gstValue);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetServiceMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetServiceMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetServiceMasterById(int serviceId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetServiceMasterById");
                objSql.AddParameter(objCmd, "@ServiceId", SqlDbType.Int, ParameterDirection.Input, 20, serviceId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteServiceMaster(int serviceId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteServiceMaster");
                objSql.AddParameter(objCmd, "@ServiceId", SqlDbType.Int, ParameterDirection.Input, 20, serviceId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion


        #region CompanyMaster
        public string InsertCompanyMaster(int companyId, string companyName, string mobileNo, string email, string website, int stateId, int cityId, string pincode, string address1, string address2, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertCompanyMaster");
                objSql.AddParameter(objCmd, "@companyId", SqlDbType.Int, ParameterDirection.Input, 20, companyId);
                objSql.AddParameter(objCmd, "@CompanyName", SqlDbType.VarChar, ParameterDirection.Input, 100, companyName);
                objSql.AddParameter(objCmd, "@MobileNo", SqlDbType.VarChar, ParameterDirection.Input, 20, mobileNo);
                objSql.AddParameter(objCmd, "@Email", SqlDbType.VarChar, ParameterDirection.Input, 50, email);
                objSql.AddParameter(objCmd, "@Website", SqlDbType.VarChar, ParameterDirection.Input, 50, website);
                objSql.AddParameter(objCmd, "@StateId", SqlDbType.Int, ParameterDirection.Input, 20, stateId);
                objSql.AddParameter(objCmd, "@CityId", SqlDbType.Int, ParameterDirection.Input, 20, cityId);
                objSql.AddParameter(objCmd, "@Pincode", SqlDbType.VarChar, ParameterDirection.Input, 50, pincode);
                objSql.AddParameter(objCmd, "@Address1", SqlDbType.VarChar, ParameterDirection.Input, -1, address1);
                objSql.AddParameter(objCmd, "@Address2", SqlDbType.VarChar, ParameterDirection.Input, -1, address2);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetCompanyMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetCompanyMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetCompanyMasterById(int companyId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetCompanyMasterById");
                objSql.AddParameter(objCmd, "@CompanyId", SqlDbType.Int, ParameterDirection.Input, 20, companyId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteCompanyMaster(int companyId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteCompanyMaster");
                objSql.AddParameter(objCmd, "@CompanyId", SqlDbType.Int, ParameterDirection.Input, 20, companyId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetStateMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetStateMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public DataTable GetDistrictMasterByStateId(int stateId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetDistrictMasterByStateId");
                objSql.AddParameter(objCmd, "@StateId", SqlDbType.Int, ParameterDirection.Input, 20, stateId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        #endregion

        #region UserMaster
        public string InsertUserMaster(int userId, int employeeId, string employeeName, string userType, string userName,
            string password, string confirmPassword, string allocateMenu, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertUserMaster");
                objSql.AddParameter(objCmd, "@UserId", SqlDbType.Int, ParameterDirection.Input, 20, userId);
                objSql.AddParameter(objCmd, "@EmployeeId", SqlDbType.Int, ParameterDirection.Input, 20, employeeId);
                objSql.AddParameter(objCmd, "@EmployeeName", SqlDbType.VarChar, ParameterDirection.Input, 50, employeeName);
                objSql.AddParameter(objCmd, "@UserType", SqlDbType.VarChar, ParameterDirection.Input, 50, userType);
                objSql.AddParameter(objCmd, "@UserName", SqlDbType.VarChar, ParameterDirection.Input, 50, userName);
                objSql.AddParameter(objCmd, "@Password", SqlDbType.VarChar, ParameterDirection.Input, 50, password);
                objSql.AddParameter(objCmd, "@ConfirmPassword", SqlDbType.VarChar, ParameterDirection.Input, 50, confirmPassword);
                objSql.AddParameter(objCmd, "@AllocateMenu", SqlDbType.VarChar, ParameterDirection.Input, -1, allocateMenu);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetUserMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetUserMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetUserMasterById(int userId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetUserMasterById");
                objSql.AddParameter(objCmd, "@UserId", SqlDbType.Int, ParameterDirection.Input, 20, userId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteUserMaster(int userId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteUserMaster");
                objSql.AddParameter(objCmd, "@UserId", SqlDbType.Int, ParameterDirection.Input, 20, userId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetMenuMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetMenuMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        #endregion

        #region MakeMaster
        public string InsertMakeMaster(int makeId, string make, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertMakeMaster");
                objSql.AddParameter(objCmd, "@MakeId", SqlDbType.Int, ParameterDirection.Input, 20, makeId);
                objSql.AddParameter(objCmd, "@Make", SqlDbType.VarChar, ParameterDirection.Input, 50, make);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetMakeMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetMakeMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetMakeMasterById(int makeId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetMakeMasterById");
                objSql.AddParameter(objCmd, "@MakeId", SqlDbType.Int, ParameterDirection.Input, 20, makeId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteMakeMaster(int makeId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteMakeMaster");
                objSql.AddParameter(objCmd, "@MakeId", SqlDbType.Int, ParameterDirection.Input, 20, makeId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region ModelMaster
        public string InsertModelMaster(int modelId, int makeId, string make, string model, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertModelMaster");
                objSql.AddParameter(objCmd, "@ModelId", SqlDbType.Int, ParameterDirection.Input, 20, modelId);
                objSql.AddParameter(objCmd, "@MakeId", SqlDbType.Int, ParameterDirection.Input, 20, makeId);
                objSql.AddParameter(objCmd, "@Make", SqlDbType.VarChar, ParameterDirection.Input, 50, make);
                objSql.AddParameter(objCmd, "@Model", SqlDbType.VarChar, ParameterDirection.Input, 50, model);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetModelMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetModelMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetModelMasterById(int modelId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetModelMasterById");
                objSql.AddParameter(objCmd, "@ModelId", SqlDbType.Int, ParameterDirection.Input, 20, modelId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public DataTable GetModelMasterByMakeId(int makeId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetModelMasterByMakeId");
                objSql.AddParameter(objCmd, "@MakeId", SqlDbType.Int, ParameterDirection.Input, 20, makeId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteModelMaster(int modelId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteModelMaster");
                objSql.AddParameter(objCmd, "@ModelId", SqlDbType.Int, ParameterDirection.Input, 20, modelId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region PaymentModeMaster
        public string InsertPaymentModeMaster(int paymentModeId, string paymentMode, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertPaymentModeMaster");
                objSql.AddParameter(objCmd, "@PaymentModeId", SqlDbType.Int, ParameterDirection.Input, 20, paymentModeId);
                objSql.AddParameter(objCmd, "@PaymentMode", SqlDbType.VarChar, ParameterDirection.Input, 50, paymentMode);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetPaymentModeMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetPaymentModeMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
        public DataTable GetPaymentModeMasterById(int paymentModeId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetPaymentModeMasterById");
                objSql.AddParameter(objCmd, "@PaymentModeId", SqlDbType.Int, ParameterDirection.Input, 20, paymentModeId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeletePaymentModeMaster(int paymentModeId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeletePaymentModeMaster");
                objSql.AddParameter(objCmd, "@PaymentModeId", SqlDbType.Int, ParameterDirection.Input, 20, paymentModeId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }
        #endregion

        #region CustomerMaster
        public string InsertCustomerMaster(int customerId, string customerNo, string customerName, string mobileNo, string address, int createdBy, DataTable customerSubDt)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertCustomerMaster");
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, customerId);
                objSql.AddParameter(objCmd, "@CustomerNo", SqlDbType.VarChar, ParameterDirection.Input, 50, customerNo);
                objSql.AddParameter(objCmd, "@CustomerName", SqlDbType.VarChar, ParameterDirection.Input, 80, customerName);
                objSql.AddParameter(objCmd, "@MobileNo", SqlDbType.VarChar, ParameterDirection.Input, 80, mobileNo);
                objSql.AddParameter(objCmd, "@Address", SqlDbType.VarChar, ParameterDirection.Input, -1, address);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@CustomerSub", SqlDbType.Structured, ParameterDirection.Input, -1, customerSubDt);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public string InsertCustomerMasterWthoutVehicleDtls(int customerId, string customerNo, string customerName, string mobileNo, string address, int createdBy)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "InsertCustomerMasterWthoutVehicleDtls");
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, customerId);
                objSql.AddParameter(objCmd, "@CustomerNo", SqlDbType.VarChar, ParameterDirection.Input, 50, customerNo);
                objSql.AddParameter(objCmd, "@CustomerName", SqlDbType.VarChar, ParameterDirection.Input, 80, customerName);
                objSql.AddParameter(objCmd, "@MobileNo", SqlDbType.VarChar, ParameterDirection.Input, 80, mobileNo);
                objSql.AddParameter(objCmd, "@Address", SqlDbType.VarChar, ParameterDirection.Input, -1, address);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public DataTable GetCustomerMaster()
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetCustomerMaster");
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public DataTable GetCustomerMasterById(int customerId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetCustomerMasterById");
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, customerId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        public string DeleteCustomerMaster(int customerId, int createdBy, string remarks)
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "DeleteCustomerMaster");
                objSql.AddParameter(objCmd, "@CustomerId", SqlDbType.Int, ParameterDirection.Input, 20, customerId);
                objSql.AddParameter(objCmd, "@CreatedBy", SqlDbType.Int, ParameterDirection.Input, 20, createdBy);
                objSql.AddParameter(objCmd, "@Remarks", SqlDbType.VarChar, ParameterDirection.Input, -1, remarks);
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        public string GetCustomerNo()
        {
            string strRetVal = "";
            SQLDAL objSql = new SQLDAL();
            SqlDataAdapter objda = new SqlDataAdapter();
            SqlCommand objCmd = new SqlCommand();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "MasterSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetCustomerNo");
                strRetVal = objCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return strRetVal;
        }

        #endregion
    }
}
