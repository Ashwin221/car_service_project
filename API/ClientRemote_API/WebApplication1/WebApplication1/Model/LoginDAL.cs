﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.AppCode;

namespace WebApplication1.Model
{
    public class LoginDAL
    {

        public DataTable GetLoginUserDetails(string userType, string userName, string password)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "LoginSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetLoginUserDetails");
                objSql.AddParameter(objCmd, "@UserType", SqlDbType.VarChar, ParameterDirection.Input, 20, userType);
                objSql.AddParameter(objCmd, "@UserName", SqlDbType.VarChar, ParameterDirection.Input, 50, userName);
                objSql.AddParameter(objCmd, "@Password", SqlDbType.VarChar, ParameterDirection.Input, 50, password);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }
    }
}
