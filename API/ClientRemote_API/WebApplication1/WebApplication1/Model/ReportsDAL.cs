﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.AppCode;

namespace WebApplication1.Model
{
    public class ReportsDAL
    {
        #region Services
        public DataTable GetServices(string fromDate, string toDate)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ReportsSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetServices");
                objSql.AddParameter(objCmd, "@FromDate", SqlDbType.VarChar, ParameterDirection.Input, 20, fromDate);
                objSql.AddParameter(objCmd, "@ToDate", SqlDbType.VarChar, ParameterDirection.Input, 20, toDate);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        #endregion

        #region Product Sales
        public DataTable GetProductSales(string fromDate, string toDate)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ReportsSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetProductSales");
                objSql.AddParameter(objCmd, "@FromDate", SqlDbType.VarChar, ParameterDirection.Input, 20, fromDate);
                objSql.AddParameter(objCmd, "@ToDate", SqlDbType.VarChar, ParameterDirection.Input, 20, toDate);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        #endregion

        #region Services By EmpId
        public DataTable GetServicesByEmpId(string fromDate, string toDate, int employeeId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ReportsSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetServicesByEmpId");
                objSql.AddParameter(objCmd, "@FromDate", SqlDbType.VarChar, ParameterDirection.Input, 20, fromDate);
                objSql.AddParameter(objCmd, "@ToDate", SqlDbType.VarChar, ParameterDirection.Input, 20, toDate);
                objSql.AddParameter(objCmd, "@EmployeeId", SqlDbType.Int, ParameterDirection.Input, 20, employeeId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        #endregion

        #region Product Sales By EmpId
        public DataTable GetProductSalesByEmpId(string fromDate, string toDate, int employeeId)
        {
            DataTable dt = new DataTable();
            SQLDAL objSql = new SQLDAL();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter objda = new SqlDataAdapter();
            try
            {
                objCmd = objSql.CreateCommand(objCmd, CommandType.StoredProcedure, "ReportsSP");
                objSql.AddParameter(objCmd, "@Action", SqlDbType.VarChar, ParameterDirection.Input, 100, "GetProductSalesByEmpId");
                objSql.AddParameter(objCmd, "@FromDate", SqlDbType.VarChar, ParameterDirection.Input, 20, fromDate);
                objSql.AddParameter(objCmd, "@ToDate", SqlDbType.VarChar, ParameterDirection.Input, 20, toDate);
                objSql.AddParameter(objCmd, "@EmployeeId", SqlDbType.Int, ParameterDirection.Input, 20, employeeId);
                objda.SelectCommand = objCmd;
                objda.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            finally
            {
                if (!(objCmd == null))
                {
                    objCmd.Dispose();
                    if (objSql != null)
                        objSql.Dispose();
                    if (objda != null)
                        objda.Dispose();
                }
            }
            return dt;
        }

        #endregion
    }
}
