﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class Master
    {
        public int unitId { get; set; }
        public string unitCode { get; set; }
        public string unitName { get; set; }


        public int gstId { get; set; }
        public string gstName { get; set; }
        public string gstValue { get; set; }


        public int productId { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
        public string description { get; set; }
        public string price { get; set; }


        public int employeeId { get; set; }
        public string employeeCode { get; set; }
        public string employeeName { get; set; }
        public string address { get; set; }
        public string mobileNo { get; set; }


        public int serviceId { get; set; }
        public string category { get; set; }
        public string serviceType { get; set; }

        public int companyId { get; set; }
        public string companyName { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public int stateId { get; set; }
        public int cityId { get; set; }
        public string pincode { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }

        public int userId { get; set; }
        public string userType { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string confirmPassword { get; set; }
        public string allocateMenu { get; set; }


        public int makeId { get; set; }
        public string make { get; set; }


        public int modelId { get; set; }
        public string model { get; set; }


        public int paymentModeId { get; set; }
        public string paymentMode { get; set; }

        public int customerId { get; set; }
        public string customerNo { get; set; }
        public string customerName { get; set; }
        public bool visible { get; set; }

        public List<customerSub> customerSub { get; set; }


        public int createdBy { get; set; }
        public string remarks { get; set; }
    }
    public class customerSub
    {
        public int makeId { get; set; }
        public string make { get; set; }
        public int modelId { get; set; }
        public string model { get; set; }
        public string vehicleNo { get; set; }
    }
}
