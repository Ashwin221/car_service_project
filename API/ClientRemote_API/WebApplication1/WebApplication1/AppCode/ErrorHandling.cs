﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.AppCode
{
    public class ErrorHandling
    {
        private static IHostingEnvironment _hostingEnvironment;

        public static object WriteError(string Exception)
        {

            try
            {
                DateTime dt = new DateTime();
                dt = DateTime.Now.Date;
                string LOGDATE = null;
                string strPath = null;
                strPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "ErrorLog");
                System.IO.Directory.CreateDirectory(strPath);
                LOGDATE = dt.ToString("MMM d yyyy");
                StreamWriter stwriter = new StreamWriter(strPath + "/" + "ErrorLog" + LOGDATE + ".txt", true);
                stwriter.WriteLine("*********************************************************************");
                stwriter.WriteLine("");
                stwriter.WriteLine("DATE TIME  :" + DateTime.Now.ToString());
                stwriter.WriteLine("ERROR INFO :" + Exception.ToString());
                stwriter.WriteLine("");
                stwriter.WriteLine("");
                stwriter.Flush();
                stwriter.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
