﻿using WebApplication1.AppCode;
using WebApplication1.Model;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System;
using System.Linq;

namespace CarWashWebAPI1.Controllers
{
    public class ServiceProductController : Controller
    {
        string str = "";
        bool result = false;
        ServiceProductDAL objDAL = new ServiceProductDAL();
        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [Route("ServiceProduct/InsertServiceProduct")]
        public JsonResult InsertServiceProduct([FromBody] ServiceProduct M)
        {
            try
            {
                DataTable serviceProductSubDt = new DataTable();

                serviceProductSubDt.Columns.Add("LineNum", typeof(int));
                serviceProductSubDt.Columns.Add("Category", typeof(string));
                serviceProductSubDt.Columns.Add("Type", typeof(string));
                serviceProductSubDt.Columns.Add("TypeId", typeof(int));
                serviceProductSubDt.Columns.Add("TypeName", typeof(string));
                serviceProductSubDt.Columns.Add("Price", typeof(string));
                serviceProductSubDt.Columns.Add("Qty", typeof(int));
                serviceProductSubDt.Columns.Add("Amount", typeof(string));
                serviceProductSubDt.Columns.Add("Discount", typeof(string));
                serviceProductSubDt.Columns.Add("IsAvailGst", typeof(bool));
                serviceProductSubDt.Columns.Add("GstId", typeof(int));
                serviceProductSubDt.Columns.Add("GstName", typeof(string));
                serviceProductSubDt.Columns.Add("GstValue", typeof(string));
                serviceProductSubDt.Columns.Add("TaxableAmt", typeof(string));
                serviceProductSubDt.Columns.Add("Total", typeof(string));
                serviceProductSubDt.Columns.Add("FreeCarWash", typeof(bool));
                serviceProductSubDt.Columns.Add("FreeBikeWash", typeof(bool));

                int l = 1;
                foreach (var c in M.serviceProductSub)
                {
                    serviceProductSubDt.Rows.Add(new object[] {l++, c.category, c.type, c.typeId, c.typeName, c.price, c.qty, c.amount, c.discount,c.isAvailGst,c.gstId,c.gstName,c.gstValue,c.taxableAmt,c.total, c.freeCarWash, c.freeBikeWash
                  });
                }
                //DataTable dt = objDAL.InsertServiceProductdt(serviceProductSubDt, M.customerId);
                str = objDAL.InsertServiceProduct(M.service_ProductId, M.service_ProductNo,M.type, M.date, M.customerId, M.mobileNo, M.customerName, M.vehicleRegNo,
                M.makeId, M.make, M.modelId, M.model, M.deliveryDateTime, M.serviceDoneById, M.serviceDoneBy, M.salesDoneById, M.salesDoneBy, M.overallTotal, M.overAllTax, M.discFromTotal, M.finalAmount,
                M.createdBy, serviceProductSubDt);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, service_ProductId = str });
        }


        [HttpGet]
        [Route("ServiceProduct/GetVehicleRegNoDtls")]
        public JsonResult GetVehicleRegNoDtls(int CustomerId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetVehicleRegNoDtls(CustomerId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        makeId = (Int32)(x["MakeId"] ?? 0),
                        modelId = (Int32)(x["ModelId"] ?? 0),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                        vehicleNo = (x["VehicleNo"] ?? "").ToString(),

                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getVehicleRegNoDtls = lst });
        }


        [HttpGet]
        [Route("ServiceProduct/GetProductServiceDtls")]
        public JsonResult GetProductServiceDtls()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetProductServiceDtls();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        serviceProductId = (Int32)(x["Service_ProductId"] ?? 0),
                        serviceProductNo = (x["Service_ProductNo"] ?? "").ToString(),
                        date = (x["Date"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        vehicleRegNo = (x["VehicleRegNo"] ?? "").ToString(),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                        serviceDoneBy = (x["ServiceDoneBy"] ?? "").ToString(),
                        salesDoneBy = (x["SalesDoneBy"] ?? "").ToString(),
                        deliveryDateTime = (x["DeliveryDateTime"] ?? "").ToString(),
                        finalInvAmt = (x["FinalInvAmt"] ?? "").ToString(),

                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getProductServiceDtls = lst });
        }

        [HttpGet]
        [Route("ServiceProduct/GetProductServiceDtlsById")]
        public JsonResult GetProductServiceDtlsById(int serviceProductId)
        {
            var main = new object();
            var sub = new object();
            try
            {
                DataSet dt = objDAL.GetProductServiceDtlsById(serviceProductId);

                main = dt.Tables[0].AsEnumerable().Select(x => new
                {
                    serviceProductId = (Int32)(x["Service_ProductId"] ?? 0),
                    serviceProductNo = (x["Service_ProductNo"] ?? "").ToString(),
                    type = (x["Type"] ?? "").ToString(),
                    date = (x["Date"] ?? "").ToString(),
                    customerId = (Int32)(x["CustomerId"] ?? 0),
                    mobileNo = (x["MobileNo"] ?? "").ToString(),
                    vehicleRegNo = (x["VehicleRegNo"] ?? "").ToString(),
                    makeId = (Int32)(x["MakeId"] ?? 0),
                    modelId = (Int32)(x["ModelId"] ?? 0),
                    deliveryDateTime = (x["DeliveryDateTime"] ?? "").ToString(),
                    //serviceDoneById = (Int32)(x["ServiceDoneById"] ?? 0),
                    //salesDoneById = (Int32)(x["SalesDoneById"] ?? 0),
                    salesDoneById = (x["SalesDoneById"] ?? "").ToString(),
                    serviceDoneById = (x["ServiceDoneById"] ?? "").ToString(),
                    overAllTotal = (x["OverAllTotal"] ?? "").ToString(),
                    discountFromTotal = (x["DiscountFromTotal"] ?? "").ToString(),
                    finalInvAmt = (x["FinalInvAmt"] ?? "").ToString(),
                }).ToList();

                sub = dt.Tables[1].AsEnumerable().Select(x => new
                {
                    serviceProductId = (Int32)(x["Service_ProductId"] ?? 0),
                    category = (x["Category"] ?? "").ToString(),
                    type = (x["Type"] ?? "").ToString(),
                    typeId = (Int32)(x["TypeId"] ?? 0),
                    typeName = (x["TypeName"] ?? "").ToString(),
                    price = (x["Price"] ?? "").ToString(),
                    qty = (Int32)(x["Qty"] ?? 0),
                    amount = (x["Amount"] ?? "").ToString(),
                    discount = (x["Discount"] ?? "").ToString(),
                    isAvailGst = (bool)(x["IsAvailGst"] ?? 0),
                    gstId = (Int32)(x["GstId"] ?? 0),
                    gstName = (x["GstName"] ?? "").ToString(),
                    gstValue = (x["GstValue"] ?? "").ToString(),
                    taxableAmt = (x["TaxableAmt"] ?? "").ToString(),
                    total = (x["Total"] ?? "").ToString(),
                    freeWash_Status = (x["FreeWash_Status"] ?? "").ToString(),
                }).ToList();

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getProductServiceDtlsByIdMain = main, getProductServiceDtlsByIdSub = sub });
        }

        [HttpGet]
        [Route("ServiceProduct/DeleteProductServiceMaster")]
        public JsonResult DeleteProductServiceMaster(int serviceProductId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteProductServiceMaster(serviceProductId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("ServiceProduct/EligibleForFreeWash")]
        public JsonResult EligibleForFreeWash(string mobileNo, string category)
        {
            try
            {
                str = objDAL.EligibleForFreeWash(mobileNo, category);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, content = str });
        }
    }
}
