﻿using WebApplication1.AppCode;
using WebApplication1.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CarWashWebAPI1.Controllers
{
    public class MasterController : Controller
    {
        string str = "";
        bool result = false;
        MasterDAL objDAL = new MasterDAL();
        public IActionResult Index()
        {
            return View();
        }

        #region UnitMaster

        [Route("Master/InsertUnitMaster")]
        [HttpPost]
        public JsonResult InsertUnitMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertUnitMaster(M.unitId, M.unitCode, M.unitName, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetUnitMaster")]
        public JsonResult GetUnitMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetUnitMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        unitId = (Int32)(x["UnitId"] ?? 0),
                        unitCode = (x["UnitCode"] ?? "").ToString(),
                        unitName = (x["UnitName"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getUnitMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetUnitMasterById")]
        public JsonResult GetUnitMasterById(int unitId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetUnitMasterById(unitId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        unitId = (Int32)(x["UnitId"] ?? 0),
                        unitCode = (x["UnitCode"] ?? "").ToString(),
                        unitName = (x["UnitName"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getUnitMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteUnitMaster")]
        public JsonResult DeleteUnitMaster(int unitId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteUnitMaster(unitId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion

        #region GStMaster

        [Route("Master/InsertGSTMaster")]
        [HttpPost]
        public JsonResult InsertGSTMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertGSTMaster(M.gstId, M.gstName, M.gstValue, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetGSTMaster")]
        public JsonResult GetGSTMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetGSTMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        gstId = (Int32)(x["GSTId"] ?? 0),
                        gstName = (x["GSTName"] ?? "").ToString(),
                        gstValue = (x["GSTValue"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getGSTMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetGSTMasterById")]
        public JsonResult GetGSTMasterById(int gstId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetGSTMasterById(gstId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        gstId = (Int32)(x["GSTId"] ?? 0),
                        gstName = (x["GSTName"] ?? "").ToString(),
                        gstValue = (x["GSTValue"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getGSTMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteGSTMaster")]
        public JsonResult DeleteGSTMaster(int gstId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteGSTMaster(gstId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion

        #region ProductMaster

        [Route("Master/InsertProductMaster")]
        [HttpPost]
        public JsonResult InsertProductMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertProductMaster(M.productId, M.productCode, M.productName, M.description, M.unitId,
                    M.unitName, M.price, M.gstId, M.gstName, M.gstValue, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetProductMaster")]
        public JsonResult GetProductMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetProductMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        productId = (Int32)(x["ProductId"] ?? 0),
                        productCode = (x["ProductCode"] ?? "").ToString(),
                        productName = (x["ProductName"] ?? "").ToString(),
                        description = (x["Description"] ?? "").ToString(),
                        unitName = (x["UnitName"] ?? "").ToString(),
                        gstId = (Int32)(x["GstId"] ?? 0),
                        gstName = (x["GSTName"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getProductMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetProductMasterById")]
        public JsonResult GetProductMasterById(int productId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetProductMasterById(productId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        productId = (Int32)(x["ProductId"] ?? 0),
                        productCode = (x["ProductCode"] ?? "").ToString(),
                        productName = (x["ProductName"] ?? "").ToString(),
                        description = (x["Description"] ?? "").ToString(),
                        unitId = (Int32)(x["UnitId"] ?? 0),
                        unitName = (x["UnitName"] ?? "").ToString(),
                        gstId = (Int32)(x["GSTId"] ?? 0),
                        gstName = (x["GSTName"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getProductMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteProductMaster")]
        public JsonResult DeleteProductMaster(int productId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteProductMaster(productId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion

        #region EmployeeMaster

        [Route("Master/InsertEmployeeMaster")]
        [HttpPost]
        public JsonResult InsertEmployeeMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertEmployeeMaster(M.employeeId, M.employeeCode, M.employeeName, M.address, M.mobileNo, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetEmployeeMaster")]
        public JsonResult GetEmployeeMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetEmployeeMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        employeeId = (Int32)(x["EmployeeId"] ?? 0),
                        employeeCode = (x["EmployeeCode"] ?? "").ToString(),
                        employeeName = (x["EmployeeName"] ?? "").ToString(),
                        address = (x["Address"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getEmployeeMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetEmployeeMasterById")]
        public JsonResult GetEmployeeMasterById(int employeeId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetEmployeeMasterById(employeeId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        employeeId = (Int32)(x["EmployeeId"] ?? 0),
                        employeeCode = (x["EmployeeCode"] ?? "").ToString(),
                        employeeName = (x["EmployeeName"] ?? "").ToString(),
                        address = (x["Address"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getEmployeeMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteEmployeeMaster")]
        public JsonResult DeleteEmployeeMaster(int employeeId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteEmployeeMaster(employeeId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion

        #region ServiceMaster

        [Route("Master/InsertServiceMaster")]
        [HttpPost]
        public JsonResult InsertServiceMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertServiceMaster(M.serviceId, M.category, M.serviceType, M.price, M.gstId, M.gstName, M.gstValue, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetServiceMaster")]
        public JsonResult GetServiceMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetServiceMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        serviceId = (Int32)(x["ServiceId"] ?? 0),
                        category = (x["Category"] ?? "").ToString(),
                        ServiceType = (x["ServiceType"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                        gstId = (Int32)(x["GstId"] ?? 0),
                        gstName = (x["GSTName"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getServiceMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetServiceMasterById")]
        public JsonResult GetServiceMasterById(int serviceId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetServiceMasterById(serviceId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        serviceId = (Int32)(x["ServiceId"] ?? 0),
                        category = (x["Category"] ?? "").ToString(),
                        serviceType = (x["ServiceType"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                        gstId = (Int32)(x["GSTId"] ?? 0),
                        gstName = (x["GSTName"] ?? "").ToString(),
                        gstValue = (x["GSTValue"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getServiceMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteServiceMaster")]
        public JsonResult DeleteServiceMaster(int serviceId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteServiceMaster(serviceId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion

        #region CompanyMaster

        [Route("Master/InsertCompanyMaster")]
        [HttpPost]
        public JsonResult InsertCompanyMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertCompanyMaster(M.companyId, M.companyName, M.mobileNo, M.email, M.website, M.stateId, M.cityId, M.pincode, M.address1, M.address2, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetCompanyMaster")]
        public JsonResult GetCompanyMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetCompanyMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        companyId = (Int32)(x["CompanyId"] ?? 0),
                        companyName = (x["CompanyName"] ?? "").ToString(),
                        state = (x["State"] ?? "").ToString(),
                        city = (x["City"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getCompanyMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetCompanyMasterById")]
        public JsonResult GetCompanyMasterById(int companyId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetCompanyMasterById(companyId);
                if (dt.Rows.Count > 0)
                {//CompanyId, CompanyName, MobileNo, Email, Website, StateId, CityId, Pincode, Address1, Address2 
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        companyId = (Int32)(x["CompanyId"] ?? 0),
                        companyName = (x["CompanyName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        email = (x["Email"] ?? "").ToString(),
                        website = (x["Website"] ?? "").ToString(),
                        stateId = (Int32)(x["StateId"] ?? 0),
                        cityId = (Int32)(x["CityId"] ?? 0),
                        pincode = (x["Pincode"] ?? "").ToString(),
                        address1 = (x["Address1"] ?? "").ToString(),
                        address2 = (x["Address2"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getCompanyMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteCompanyMaster")]
        public JsonResult DeleteCompanyMaster(int companyId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteCompanyMaster(companyId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetStateMaster")]
        public JsonResult GetStateMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetStateMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        stateId = (Int32)(x["StateId"] ?? 0),
                        state = (x["State"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getStateMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetDistrictMasterByStateId")]
        public JsonResult GetDistrictMasterByStateId(int stateId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetDistrictMasterByStateId(stateId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        cityId = (Int32)(x["CityId"] ?? 0),
                        stateId = (Int32)(x["StateId"] ?? 0),
                        city = (x["City"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getDistrictMasterByStateId = lst });
        }

        #endregion

        #region UserMaster

        [Route("Master/InsertUserMaster")]
        [HttpPost]
        public JsonResult InsertUserMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertUserMaster(M.userId, M.employeeId, M.employeeName, M.userType, M.userName,
            M.password, M.confirmPassword, M.allocateMenu, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetUserMaster")]
        public JsonResult GetUserMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetUserMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        userId = (Int32)(x["UserId"] ?? 0),
                        employeeId = (Int32)(x["EmployeeId"] ?? 0),
                        employeeName = (x["EmployeeName"] ?? "").ToString(),
                        userType = (x["UserType"] ?? "").ToString(),
                        userName = (x["UserName"] ?? "").ToString(),
                        allocateMenu = (x["AllocateMenu"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getUserMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetUserMasterById")]
        public JsonResult GetUserMasterById(int userId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetUserMasterById(userId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        userId = (Int32)(x["UserId"] ?? 0),
                        employeeId = (Int32)(x["EmployeeId"] ?? 0),
                        employeeName = (x["EmployeeName"] ?? "").ToString(),
                        userType = (x["UserType"] ?? "").ToString(),
                        userName = (x["UserName"] ?? "").ToString(),
                        Password = (x["Password"] ?? "").ToString(),
                        confirmPassword = (x["ConfirmPassword"] ?? "").ToString(),
                        allocateMenu = (x["AllocateMenu"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getUserMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteUserMaster")]
        public JsonResult DeleteUserMaster(int userId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteUserMaster(userId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetMenuMaster")]
        public JsonResult GetMenuMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetMenuMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        menuId = (Int32)(x["MenuId"] ?? 0),
                        header = (x["Header"] ?? "").ToString(),
                        menuName = (x["MenuName"] ?? "").ToString(),
                        subMenuIds = (x["SubMenuIds"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getMenuMaster = lst });
        }

        #endregion

        #region MakeMaster

        [Route("Master/InsertMakeMaster")]
        [HttpPost]
        public JsonResult InsertMakeMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertMakeMaster(M.makeId, M.make, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetMakeMaster")]
        public JsonResult GetMakeMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetMakeMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        makeId = (Int32)(x["MakeId"] ?? 0),
                        make = (x["Make"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getMakeMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetMakeMasterById")]
        public JsonResult GetMakeMasterById(int makeId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetMakeMasterById(makeId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        makeId = (Int32)(x["MakeId"] ?? 0),
                        make = (x["Make"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getMakeMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteMakeMaster")]
        public JsonResult DeleteMakeMaster(int makeId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteMakeMaster(makeId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion 

        #region ModelMaster

        [Route("Master/InsertModelMaster")]
        [HttpPost]
        public JsonResult InsertModelMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertModelMaster(M.modelId, M.makeId, M.make, M.model, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetModelMaster")]
        public JsonResult GetModelMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetModelMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        modelId = (Int32)(x["ModelId"] ?? 0),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getModelMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetModelMasterById")]
        public JsonResult GetModelMasterById(int modelId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetModelMasterById(modelId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        modelId = (Int32)(x["ModelId"] ?? 0),
                        makeId = (Int32)(x["MakeId"] ?? 0),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getModelMasterById = lst });
        }

        [HttpGet]
        [Route("Master/GetModelMasterByMakeId")]
        public JsonResult GetModelMasterByMakeId(int makeId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetModelMasterByMakeId(makeId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        modelId = (Int32)(x["ModelId"] ?? 0),
                        makeId = (Int32)(x["MakeId"] ?? 0),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getModelMasterByMakeId = lst });
        }

        [HttpGet]
        [Route("Master/DeleteModelMaster")]
        public JsonResult DeleteModelMaster(int modelId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteModelMaster(modelId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion 

        #region PaymentModeMaster

        [Route("Master/InsertPaymentModeMaster")]
        [HttpPost]
        public JsonResult InsertPaymentModeMaster([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertPaymentModeMaster(M.paymentModeId, M.paymentMode, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetPaymentModeMaster")]
        public JsonResult GetPaymentModeMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetPaymentModeMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        paymentModeId = (Int32)(x["PaymentModeId"] ?? 0),
                        paymentMode = (x["PaymentMode"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getPaymentModeMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetPaymentModeMasterById")]
        public JsonResult GetPaymentModeMasterById(int paymentModeId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetPaymentModeMasterById(paymentModeId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        paymentModeId = (Int32)(x["PaymentModeId"] ?? 0),
                        paymentMode = (x["PaymentMode"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getPaymentModeMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeletePaymentModeMaster")]
        public JsonResult DeletePaymentModeMaster(int paymentModeId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeletePaymentModeMaster(paymentModeId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        #endregion

        #region CustomerMaster

        [HttpPost]
        [Route("Master/InsertCustomerMaster")]
        public JsonResult InsertCustomerMaster([FromBody] Master M)
        {
            try
            {
                DataTable customerSubDt = new DataTable();
                customerSubDt.Columns.Add("LineNum", typeof(int));
                customerSubDt.Columns.Add("MakeId", typeof(int));
                customerSubDt.Columns.Add("Make", typeof(string));
                customerSubDt.Columns.Add("ModelId", typeof(int));
                customerSubDt.Columns.Add("Model", typeof(string));
                customerSubDt.Columns.Add("VehicleNo", typeof(string));

                int l = 1;
                foreach (var c in M.customerSub)
                {
                    customerSubDt.Rows.Add(new object[] { l++, c.makeId, c.make, c.modelId, c.model, c.vehicleNo });
                }
                str = objDAL.InsertCustomerMaster(M.customerId, M.customerNo, M.customerName, M.mobileNo, M.address, M.createdBy, customerSubDt);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpPost]
        [Route("Master/InsertCustomerMasterWthoutVehicleDtls")]
        public JsonResult InsertCustomerMasterWthoutVehicleDtls([FromBody] Master M)
        {
            try
            {
                str = objDAL.InsertCustomerMasterWthoutVehicleDtls(M.customerId, M.customerNo, M.customerName, M.mobileNo, M.address, M.createdBy);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetCustomerMaster")]
        public JsonResult GetCustomerMaster()
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetCustomerMaster();
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        customerNo = (x["CustomerNo"] ?? "").ToString(),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        address = (x["Address"] ?? "").ToString()
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getCustomerMaster = lst });
        }

        [HttpGet]
        [Route("Master/GetCustomerMasterById")]
        public JsonResult GetCustomerMasterById(int customerId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetCustomerMasterById(customerId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        customerNo = (x["CustomerNo"] ?? "").ToString(),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        address = (x["Address"] ?? "").ToString(),
                        makeId = (Int32)(x["MakeId"] ?? 0),
                        make = (x["Make"] ?? "").ToString(),
                        modelId = (Int32)(x["ModelId"] ?? 0),
                        model = (x["Model"] ?? "").ToString(),
                        vehicleNo = (x["VehicleNo"] ?? "").ToString(),
                        visible = (bool)(x["Visible"] ?? 0)
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getCustomerMasterById = lst });
        }

        [HttpGet]
        [Route("Master/DeleteCustomerMaster")]
        public JsonResult DeleteCustomerMaster(int customerId, int createdBy, string remarks)
        {
            try
            {
                str = objDAL.DeleteCustomerMaster(customerId, createdBy, remarks);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result });
        }

        [HttpGet]
        [Route("Master/GetCustomerNo")]
        public JsonResult GetCustomerNo()
        {
            try
            {
                str = objDAL.GetCustomerNo();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, customerNo = str });
        }



        #endregion 


    }
}
