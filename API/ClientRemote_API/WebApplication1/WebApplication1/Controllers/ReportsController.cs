﻿using WebApplication1.AppCode;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System;
using WebApplication1.Model;
using System.Linq;

namespace CarWashWebAPI1.Controllers
{
    public class ReportsController : Controller
    {
        ReportsDAL objDAL = new ReportsDAL();
        bool result;
        public IActionResult Index()
        {
            return View();
        }

        #region Services

        [HttpGet]
        [Route("Reports/GetServices")]
        public JsonResult GetServices(string fromDate, string toDate)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetServices(fromDate, toDate);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        service_ProductId = (Int32)(x["Service_ProductId"] ?? 0),
                        date = (x["Date"] ?? "").ToString(),
                        serviceProductNo = (x["Service_ProductNo"] ?? "").ToString(),
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        vehicleRegNo = (x["VehicleRegNo"] ?? "").ToString(),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                        typeName = (x["TypeName"] ?? "").ToString(),
                        serviceDoneBy = (x["ServiceDoneBy"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                        gstName = (x["GstName"] ?? "").ToString(),
                        finalInvAmt = (x["FinalInvAmt"] ?? "").ToString(),
                        deliveryDateTime = (x["DeliveryDateTime"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getServices = lst });
        }

        #endregion

        #region Product Sales

        [HttpGet]
        [Route("Reports/GetProductSales")]
        public JsonResult GetProductSales(string fromDate, string toDate)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetProductSales(fromDate, toDate);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        service_ProductId = (Int32)(x["Service_ProductId"] ?? 0),
                        date = (x["Date"] ?? "").ToString(),
                        serviceProductNo = (x["Service_ProductNo"] ?? "").ToString(),
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        vehicleRegNo = (x["VehicleRegNo"] ?? "").ToString(),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                        typeName = (x["TypeName"] ?? "").ToString(),
                        salesDoneBy = (x["SalesDoneBy"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                        gstName = (x["GstName"] ?? "").ToString(),
                        finalInvAmt = (x["FinalInvAmt"] ?? "").ToString(),
                        deliveryDateTime = (x["DeliveryDateTime"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getProductSales = lst });
        }

        #endregion

        #region Services By EmpId

        [HttpGet]
        [Route("Reports/GetServicesByEmpId")]
        public JsonResult GetServicesByEmpId(string fromDate, string toDate, int employeeId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetServicesByEmpId(fromDate, toDate, employeeId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        service_ProductId = (Int32)(x["Service_ProductId"] ?? 0),
                        date = (x["Date"] ?? "").ToString(),
                        serviceProductNo = (x["Service_ProductNo"] ?? "").ToString(),
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        vehicleRegNo = (x["VehicleRegNo"] ?? "").ToString(),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                        typeName = (x["TypeName"] ?? "").ToString(),
                        serviceDoneBy = (x["ServiceDoneBy"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                        gstName = (x["GstName"] ?? "").ToString(),
                        finalInvAmt = (x["FinalInvAmt"] ?? "").ToString(),
                        deliveryDateTime = (x["DeliveryDateTime"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getServicesByEmpId = lst });
        }

        #endregion

        #region Product Sales By EmpId

        [HttpGet]
        [Route("Reports/GetProductSalesByEmpId")]
        public JsonResult GetProductSalesByEmpId(string fromDate, string toDate, int employeeId)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetProductSalesByEmpId(fromDate, toDate, employeeId);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        service_ProductId = (Int32)(x["Service_ProductId"] ?? 0),
                        date = (x["Date"] ?? "").ToString(),
                        serviceProductNo = (x["Service_ProductNo"] ?? "").ToString(),
                        customerId = (Int32)(x["CustomerId"] ?? 0),
                        customerName = (x["CustomerName"] ?? "").ToString(),
                        mobileNo = (x["MobileNo"] ?? "").ToString(),
                        vehicleRegNo = (x["VehicleRegNo"] ?? "").ToString(),
                        make = (x["Make"] ?? "").ToString(),
                        model = (x["Model"] ?? "").ToString(),
                        typeName = (x["TypeName"] ?? "").ToString(),
                        salesDoneBy = (x["SalesDoneBy"] ?? "").ToString(),
                        price = (x["Price"] ?? "").ToString(),
                        gstName = (x["GstName"] ?? "").ToString(),
                        finalInvAmt = (x["FinalInvAmt"] ?? "").ToString(),
                        deliveryDateTime = (x["DeliveryDateTime"] ?? "").ToString(),
                    }).ToList();
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getProductSalesByEmpId = lst });
        }

        #endregion
    }

}
