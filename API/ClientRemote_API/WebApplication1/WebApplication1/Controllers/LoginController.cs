﻿using WebApplication1.AppCode;
using WebApplication1.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Linq;

namespace CarWashWebAPI1.Controllers
{
    public class LoginController : Controller
    {
        string str = "";
        bool result = false;
        LoginDAL objDAL = new LoginDAL();
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("Login/GetLoginUserDetails")]
        public JsonResult GetLoginUserDetails(string userType, string userName, string password)
        {
            var lst = new object();
            try
            {
                DataTable dt = objDAL.GetLoginUserDetails(userType, userName, password);
                if (dt.Rows.Count > 0)
                {
                    lst = dt.AsEnumerable().Select(x => new
                    {
                        userId = (Int32)(x["UserId"] ?? 0),
                        employeeId = (Int32)(x["EmployeeId"] ?? 0),
                        employeeName = (x["EmployeeName"] ?? "").ToString(),
                        userType = (x["UserType"] ?? "").ToString(),
                        userName = (x["UserName"] ?? "").ToString(),
                        password = (x["Password"] ?? "").ToString(),
                        allocateMenu = (x["AllocateMenu"] ?? "").ToString()
                    }).ToList();
                    result = true;
                }
                else
                {
                    lst = new DataTable().AsEnumerable().ToList();
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
                ErrorHandling.WriteError(ex.Message.ToString() + "\r\n" + ex.StackTrace);
            }
            return Json(new { success = result, getLoginUserDetails = lst });
        }
    }
}
